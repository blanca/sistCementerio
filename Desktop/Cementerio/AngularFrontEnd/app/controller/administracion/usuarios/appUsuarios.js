app.controller('usuariosController', function ($scope, CONFIG, DreamFactory,sessionService,$route,ngTableParams,$filter,sweet) {
    
    //**** listado usando procedimientos almacenados
    $scope.getUsuarios = function(){
        $.blockUI();
        var resUsuario = {
            "procedure_name":"usuarioslst"
        };
        //servicio listar usuarios
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resUsuario);
        obj.success(function(response){
            $scope.obtUsuarios = response;
            var data = response;   //grabamos la respuesta para el paginado
            $scope.tablaUsuarios = new ngTableParams({
                page: 1,          
                count: 10,  
                filter: {},
                sorting: {}    
            }, {
                total: $scope.obtUsuarios.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtUsuarios, params.filter()) :
                    $scope.obtUsuarios;              
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.obtUsuarios;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
            $.unblockUI(); 
        })
        obj.error(function(error) {
            $scope.errors["error_usuario"] = error;            
        });
    };
    $scope.getPersonasUsuarios = function(a,b){
        if(a=='new'){
            var resPersonasUsuarios = {
                "procedure_name":"persona_list_no_usuario_new"
            };    
        }
        else{
            var resPersonasUsuarios = {
                "procedure_name":"persona_list_no_usuario_upd",
                "body":{"params": [{"name":"idpsr","param_type":"IN","value":b}]}
            };        
        }
        DreamFactory.api[COfNFIG.SERVICE].callStoredProcWithParams(resPersonasUsuarios)
        .success(function (response) {
            $scope.personas = response;           
        }).error(function(error) {
            $scope.errors["error_persona"] = error;            
        });
    };
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
        
    $scope.adicionarUsuario = function(datosUsuario){
        $.blockUI();
        var usuario = {}; 
        usuario['usr_prs_id'] = datosUsuario.psrId;
        usuario['usr_usuario'] = datosUsuario.usrUsuario;
        usuario['usr_clave'] = datosUsuario.usrClave;
        usuario['usr_controlar_ip'] = '1';
        usuario['usr_registrado'] = fechactual;
        usuario['usr_modificado'] = fechactual;
        usuario['usr_usr_id'] = sessionService.get('IDUSUARIO');
        usuario['usr_estado'] = 'A';
        var resUsuario = {
            table_name:"_bp_usuarios",
            body:usuario
        };
        //servicio insertar usuarios
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resUsuario);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })
    };

    $scope.modificarUsuario = function(usrId, datosUsuario){
        $.blockUI();
        var usuario = {};
        usuario['usr_prs_id'] = datosUsuario.psrId;
        usuario['usr_usuario'] = datosUsuario.usrUsuario;
        usuario['usr_clave'] = datosUsuario.usrClave;
        usuario['usr_controlar_ip'] = '1';
        usuario['usr_registrado'] = fechactual;
        usuario['usr_modificado'] = fechactual;
        usuario['usr_usr_id'] = sessionService.get('IDUSUARIO');
        
        var resUsuario = {
            table_name:"_bp_usuarios",
            id:usrId,
            body:usuario
        };
        //servicio modificar usuarios
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resUsuario);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
        })
    };
    $scope.eliminarUsuario = function(usrId){
        $.blockUI();
        var usuario = {};
        usuario['usr_modificado'] = fechactual;
        usuario['usr_estado'] = 'B';
        usuario['usr_usr_id'] = sessionService.get('IDUSUARIO');
        var resUsuario = {
            table_name:"_bp_usuarios",
            id:usrId,
            body:usuario
        };
        //servicio eliminar usuarios
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resUsuario);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
    $scope.limpiar = function(){
        $scope.getPersonasUsuarios("new");
        $scope.datosUsuario = '';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Usuarios";
    };
    $scope.modificarUsuarioCargar = function(usuario){
        $scope.getPersonasUsuarios("upd",usuario.psrId);
        $scope.datosUsuario=usuario;
        $scope.boton="upd";
        $scope.desabilitado=false;
        $scope.titulo="Modificar Usuarios";
    };
    $scope.eliminarUsuarioCargar = function(usuario){
        $scope.getPersonasUsuarios("upd",usuario.psrId);
        $scope.datosUsuario=usuario;
        $scope.desabilitado=true;
        $scope.boton="del";
        $scope.titulo="Eliminar Usuarios";
    };
    //iniciando el controlador
    $scope.$on('api:ready',function(){
        //alert('gfhdg');
        $scope.getUsuarios();       
    });
    $scope.inicioUsuarios = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.getUsuarios();
            
        }
    }; 
});
