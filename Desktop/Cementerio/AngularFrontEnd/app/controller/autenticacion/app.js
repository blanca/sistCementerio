var app = angular.module('myApp', ['ngResource','ngRoute', 'ngAnimate', 'toaster', 'ngTable', 'ngDreamFactory','ui.bootstrap','angularMoment','hSweetAlert','mwl.calendar','angularFileUpload', 'angularTreeview'])
.constant('DSP_URL', 'http://localhost:8080').constant('DSP_API_KEY', 'prueba').constant('DEV_ENV', 0);

app.config(['$httpProvider', 'DSP_API_KEY', function($httpProvider, DSP_API_KEY) {
    $httpProvider.defaults.headers.common['X-DreamFactory-Application-Name'] = DSP_API_KEY;
    }]);

//Servicio Actualización de logs
app.service('registroLog', function(DreamFactory,CONFIG){
    this.almacenarLog = function(sIdUsuario, sIdCiudadano, sIdFormulario, sEvento){
        var fecha= new Date();
        var fechactual=fecha.getFullYear() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();        
        var aLog = {};
        aLog = {
            'lgs_usr_id' : sIdUsuario,
            'lgs_dtspsl_id': sIdCiudadano,
            'lgs_frm_id': sIdFormulario,
            'lgs_evento' : sEvento,
            'lgs_registrado' : fechactual,
            'lgs_modificado' : fechactual            
        };
        var datosLog = {
            table_name:"_bp_logs_eventos",
            body:aLog
        };
        //servicio insertar usuarios
        var obj = DreamFactory.api[CONFIG.SERVICE].createRecords(datosLog);
        obj.success(function(data){
            console.log("Registro almacenado");
        })
        .error(function(data){
            console.log("Error al almacenar registro");
        });
     }
});

app.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
			});
		}
	};
}]);

///servicio para q funcione el upload
app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
        var fd = new FormData();
        fd.append('files', file);
        $http.post(uploadUrl + file.name + "?app_name=todoangular", fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);

app.controller('NavCtrl',['$scope', 'DreamFactory','$http','$rootScope','sessionService','CONFIG', function ($scope, DreamFactory, $http, $rootScope, sessionService,CONFIG) {
    "use strict";
    //funcion listarMenu
    $scope.generarMenu = function(){
        var parametros = {
                "procedure_name":"menu",
                "body":{
                        "params": [
                            {
                                "name": "idusr",
                                "value": sessionService.get('IDUSUARIO')
                            }
                        ]
                }
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(parametros).success(function (results){
            if(results.length > 0){
                //Generar el json correcto para el menu
                var sgrupo = "";
                var lstGrupos = [];
                var lstSubGrupo = [];
                var j=0;
                $.each(results, function(key, value){
                    var lstOpciones = [];
                    var sContenido = value["contenido"].replace("../", "");
                    sContenido = sContenido.replace(/\//gi, "|");            
                    lstOpciones[0] = {};
                    lstOpciones[0]["id_opcion"] = value["idopcion"];
                    lstOpciones[0]["opcion"] = value["opcion"];
                    lstOpciones[0]["contenido"] = sContenido;
                    var sOpciones = JSON.stringify(lstOpciones);
                    var sOpciones = sOpciones.substring(1, sOpciones.length)
                    var sOpciones = sOpciones.substring(0, sOpciones.length-1);     
                    if(sgrupo != value["grp"]){
                        if(sgrupo != ""){
                            lstSubGrupo = [];
                        }            
                        lstGrupos[j] = {};
                        lstGrupos[j]["id_grupo"] = value["idgrp"];
                        lstGrupos[j]["grupo"] = value["grp"];
                        lstGrupos[j]["sub_categories"] = lstOpciones;
                        sgrupo = value["grp"];
                        j = j + 1;
                    }         
                    lstGrupos[j-1]["sub_categories"] = lstSubGrupo;
                    lstSubGrupo.push(sOpciones);     
                });
                var listarMenu = JSON.stringify(lstGrupos);
                listarMenu = listarMenu.replace(/\[\"/gi, "[");
                listarMenu = listarMenu.replace(/}","{/gi, "},{");
                listarMenu = listarMenu.replace(/\"\]/gi, "]");
                listarMenu = listarMenu.replace(/\\"/gi, '"');
                //console.log(listarMenu);
                $scope.categories = JSON.parse(listarMenu);
                
            }else{
                $scope.msg = "Error en usurio y/o contraseña";
                $location.path('');
            }
            //console.log(results);                
        }).error(function(results){
            //LogGuardarInfo.alerta("World");
        }); 
    }
    //al realizar f5
    $scope.$on('api:ready',function(){            
        //$scope.generarMenu();
        setTimeout(function(){
            $scope.generarMenu();
        },2000);        
    });
    $scope.inicioMenu = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.generarMenu();
        }
    };     
}]);


app.config(['$routeProvider',
  function ($routeProvider) {
        $default = '../autenticacion/index.html';
        //$ruta = '../administracion/usuarios.html';        
        $routeProvider.
        when('/login', {
            title: 'Login',
            templateUrl: 'partials/login.html',
            controller: 'authCtrl'
        })
            .when('/logout', {
                title: 'Logout',
                templateUrl: 'partials/login.html',
                controller: 'logoutCtrl'
            })
            .when('/signup', {
                title: 'Signup',
                templateUrl: 'partials/signup.html',
                controller: 'authCtrl'
            })
            .when('/registro', {
                title: 'Registro',
                templateUrl: '../registro_ciudadano/registro/index.html',
                controller: 'authCtrl'
            })
            .when('/dashboard', {
                title: 'Dashboard',
                templateUrl: 'partials/dashboard.html',
                controller: 'authCtrl'
            })
            .when('/oauth_callback', {
            title: 'Dashboard',
            templateUrl: 'partials/oauth_callback.html',
            controller: 'authCtrl'
            })
            .when('/', {
                title: 'Login',
                templateUrl: 'partials/login.html',
                controller: 'authCtrl',
                role: '0'
            })
			.when('/:name', {
                    templateUrl: 'partials/blank.html',
                    controller: PagesController
                }) 
            .otherwise({                
                templateUrl: 'partials/nofound.html'
            });
  }])
    .run(function ($rootScope, $location, Data, sessionService,CONFIG) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            $rootScope.authenticated = false;
            $rootScope.usuario = sessionService.get('IDUSUARIO');
            $rootScope.usRol = sessionService.get('US_ROL');
            $rootScope.usNombre = sessionService.get('US_NOMBRE');
            $rootScope.usPaterno = sessionService.get('US_PATERNO');
            $rootScope.usMaterno = sessionService.get('US_MATERNO');        
            $rootScope.login_local = sessionService.get('LOGIN_LOCAL');
            $rootScope.login_oaut = localStorage.getItem("imgur");
            $rootScope.nombre = sessionService.get('USUARIO');
            
            if((!$rootScope.login_local) && (!$rootScope.login_oaut)){
                if(next.originalPath != "/oauth_callback"){                    
                    $location.path("");
                }
            }  
        });
    });
	
function PagesController($scope, $http, $route, $routeParams, $compile) {
	var cadena = $routeParams.name;	
	var res = cadena.replace(/\|/g, "/");
	console.log(cadena);
	console.log(res);
	var direccion = "../"+res;
    $route.current.templateUrl = direccion;
    $http.get($route.current.templateUrl).then(function(msg) {
        $('#ng-view').html($compile(msg.data)($scope));
    });	
}

PagesController.$inject = ['$scope', '$http', '$route', '$routeParams', '$compile'];

app.factory("MyService", function() {
		  return {
    				data: {}
       			};
  });
