app.controller('authCtrl' , function ($scope, $rootScope, $routeParams, $location, $http, Data, sessionService, CONFIG, LogGuardarInfo, DreamFactory, registroLog, sweet) {
    //inicialmente fijado esos objetos como null para evitar el error indefinido
    $scope.login = {};
    $scope.signup = {};
    $scope.doLogin = function (customer) {
        //Comentando la autenticacion de los servicios, hasta encontrar solucion al problema
        /*DreamFactory.api.user.login(CONFIG.CREDENCIAL,
              function(result) {*/
                /*var misDatos = {
                        "procedure_name":"autenticacion",
                        "body":{
                                "params": [
                                    {"name": "usrid","value": customer.usuario},
                                    {"name": "usuario","value": customer.clave}
                                ]
                        }
                };*/
                var misDatos = {
                    "procedure_name":"autenticacion",
                    "body":{
                        "params": [
                            {"name": "usrid","value": customer.usuario},
                            {"name": "usuario","value": customer.clave}
                            ]
                    }
                };
                DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(misDatos).success(function (results){                    
                    if(results.length > 0){                        
                        sessionService.set('IDUSUARIO', results[0].idusr);
                        sessionService.set('USUARIO', results[0].usr);
                        sessionService.set('US_IDROL', results[0].idrl);
                        sessionService.set('US_ROL', results[0].rol);
                        sessionService.set('ID_DIFUNTO', 0);

                        sessionService.set('NOMBRE_AGENCIA', results[0].yagc_agencia);
                        $rootScope.nombreAgenciaIdSesion = results[0].yagc_agencia;
                        $rootScope.nombre = sessionService.get('USUARIO');
                        
                        sessionService.set('US_NOMBRE', results[0].nombre);
                        sessionService.set('US_PATERNO', results[0].paterno);
                        sessionService.set('US_MATERNO', results[0].materno);
                        sessionService.set('ID_AGENCIA', results[0].yagc_id);
                        sessionService.set('LOGIN_LOCAL', "LOCAL");
                        //Registro log inicio de session usuario
                        registroLog.almacenarLog(results[0].idusr,0,0, "Inicio de sesión");
                        //Redireccionando a dashboard
                        $location.path('registro_ciudadano|datosCiudadano');
                    }else{
                        //$scope.msg = "Error en usurio y/o contraseña";
                        sweet.show('', 'Error en usurio y/o contraseña', 'error');
                        $location.path('');
                    }
                }).error(function(results){
                    //Alguna alerta si da un error
                });
              //Comentando la autenticacion de los servicios, hasta encontrar solucion al problema        
              /*},
              // Error function
              function(error) {
                // Handle Login failure
              });*/
    };    
    $scope.signup = {usuario:'',clave:'',name:'',phone:'',address:''};
    $scope.signUp = function (customer) {
        Data.post('signUp', {
            customer: customer
        }).then(function (results) {
            Data.toast(results);
            if (results.status == "success") {
                $location.path('dashboard');
            }
        });
    };
    $scope.logout = function () {
        console.log("cerrando session:");
        //ELIMINAR SESSIONES DIFERENTES
        localStorage.removeItem('imgur');        
        $scope.inicioGestionDocumental('remove');        
        setTimeout(function(){
        //Registro log, cierre de sesison usuario
        registroLog.almacenarLog(sessionService.get('IDUSUARIO'),0,0, "Cierre de sesión");
            //Destruyendo las variables de session 
            sessionService.destroy('USER');
            sessionService.destroy('IDUSUARIO');
            sessionService.destroy('USUARIO');
            sessionService.destroy('US_IDROL');
            sessionService.destroy('US_ROL');
            sessionService.destroy('US_NOMBRE');
            sessionService.destroy('US_PATERNO');
            sessionService.destroy('US_MATERNO');            
            sessionService.destroy('US_MATERNO');            
            sessionService.destroy('US_MATERNO');
            sessionService.destroy('LOGIN_LOCAL');
            $location.path('');
        },2000);
    }
    $scope.getAgencia = function () {
       var resAgencia = {
            "procedure_name":"agenciaslst"     
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resAgencia)
        .success(function (response) {      
            $scope.datos = response;
        }).error(function(error) {
            $scope.errors["error_uos"] = error;
        });    
    };
    $scope.registroCiudadano = function (customer) {
       console.log(customer);    
       //$scope.getAgencia();
    };
        
    /*cross domain*/
    /*Actualiza el localstorage cuando exista*/    
    $scope.inicioGestionDocumental = function(sEstado){
        var sData = "";
        var sDatoImgur = localStorage.getItem("imgur");
        if(sDatoImgur){
            sData = JSON.parse(sDatoImgur);
        }        
        var win = document.getElementById('sub_dominio').contentWindow;
        setTimeout(function(){         
           switch(sEstado) {
                case 'set':
                    win.postMessage(JSON.stringify({key: 'imgur', method: "set", data: sData}), "*");
                    break;
                case 'get':
                    win.postMessage(JSON.stringify({key: 'imgur', method: "get", data: sData}), "*");
                    break;                
               case 'remove':
                    win.postMessage(JSON.stringify({key: 'imgur', method: "remove", data: sData}), "*");
                    break;
                default:
                   console.log("NO HAY OPCIONES");
            }
        },2000);
    };
        
        
    
    //AL REGRESAR LA URL DESDE EL OAUT    
    $scope.inicioLogin = function () {
        var win = window;        
        try{   
            
            $scope.accessToken = JSON.parse(window.localStorage.getItem("imgur")).oauth.access_token;
            $scope.accountUsername = JSON.parse(window.localStorage.getItem("imgur")).oauth.account_username;
            
            var sAgencia = "1";            
            //CON ESTOS USUARIOS BUSCAR EN LA BDD            
            var ouatDatos = {
                "procedure_name":"autenticacionOaut",
                "body":{
                    "params": [
                        {"name": "usrid","value": $scope.accountUsername},                        
                        {"name": "idagc","value": sAgencia}
                        ]
                }
            };            
            $scope.loginOaut2(ouatDatos);
        }catch(e){}
    };
    
    $scope.$on('api:ready',function(){
        //$scope.getAgencia();
    });     
});