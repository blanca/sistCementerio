app.controller('datosCiudadanoController', function ($scope, $route,$rootScope,$location, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet,FileUploader) {
    var aRegistro = { "cedula": "","celular":"","correo":"","direccion":"","estado_civil":"",
                        "fecha_nacimiento":"","materno":"","nombre":"","ocupacion":"","paterno":"","sexo":"","telefono":""
                      };
    var aImagen =  { "titulo": "","resumen":"","fecha":"","imagen":"","url":"","registrado":"","modificado":""};
    var aEstadoC = { "idEst": "","estado_civil":""};               
    var imagenNoticias = "";
    var idEst = ""; 
    var fecha= new Date();
    var mes=fecha.getMonth()+1;
    if(mes.toString().length==1)
        mes='0'+mes;
    var dia=fecha.getDate();
    if(dia.toString().length==1)
        dia='0'+dia;
    var fechactual=fecha.getFullYear() + "-" + mes + "-" + dia + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

    var sIdPltaformista = sessionService.get('IDUSUARIO'); 
    $scope.img_url=CONFIG.IMG_URL+"/noticias";  //url de las imagenes de noticias
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
    };
    /********** datos de archivo upload**************/
    var uploader = $scope.uploader = new FileUploader({
           url: CONFIG.UPLOAD_URL+"?desripcion=noticias"

        });
        //console.log(url);
        uploader.filters.push({
            name: 'customFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            //console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
             
            //console.info('onAfterAddingFile', fileItem);
             
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            //console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
             //console.info('onBeforeUploadItem', item);
            
        };
        uploader.onProgressItem = function(fileItem, progress) {
            //console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            //console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers
            archivoUpload =fileItem._file.name;
            imagenNoticias = archivoUpload;
            //alert(archivoUpload);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };
    /*************************/
        $scope.limpiar = function(){
        $scope.noticia = [''];
        $scope.boton="new";
        $scope.desabilitado = false;
        $scope.titulo="Registrar Noticia";
        };
    /********/
       
        $scope.guardarNoticia = function(response){
        //Recuperando los datos personales
        console.log(response);
        var a = response.titulo;
        //console.log(a);
        //console.log(archivoUpload);
         var parametros = {
            "table_name":"_ciudadano_noticias",
            "body":{
                "record": [
                    {
                        "cnt_titulo": response.cnttitulo,
                        "cnt_resumen": response.cntresumen,
                        "cnt_url": response.cnturl,
                        "cnt_imagen": archivoUpload,
                        "cnt_registrado": response.cntregistrado,
                        "cnt_modificado": fechactual,
                        "cnt_visible": "SI",
                        "cnt_estado": "A",
                        "cnt_id_usuario": "1"
                    }
                ]
            }
        };
            DreamFactory.api[CONFIG.SERVICE].createRecords(parametros).success(function (results){
                if(results.record.length > 0){                
                  sweet.show('', "Registro insertado", 'success'); 
                   $route.reload();
                }else{
                   sweet.show('', "Registro no insertado", 'error'); 
                }             
            }).error(function(results){
        });

    };
    /******modificar noticia****/
     $scope.modificarNoticiaCargar = function(datos){
        console.log(datos);
        $scope.noticia = datos;
        $scope.boton="upd";
        $scope.desabilitado=false;
        $scope.titulo="Modificar Opcion";
    };
    var cntimagen = "";
    $scope.modificarNoticia = function(ntcId,datos){
        if (imagenNoticias == "") {
             imagenNoticias = datos.cntimagen;
        }
        else
        {  
           imagenNoticias= imagenNoticias;
        }
        var noticia = {}; 
        noticia['cnt_titulo'] = datos.cnttitulo;
        noticia['cnt_resumen'] = datos.cntresumen;
        noticia['cnt_url'] = datos.cnturl;
        noticia['cnt_imagen'] = imagenNoticias;
        noticia['opc_modificado'] = fechactual;
        noticia['cnt_registrado'] = datos.cntregistrado;
        noticia['cnt_id_usuario'] = sessionService.get('IDUSUARIO');
        var parametros = {"table_name":"_ciudadano_noticias", 
                        "body":noticia,
                        "id" : ntcId}; 
        var obj = DreamFactory.api[CONFIG.SERVICE].updateRecord(parametros);
        obj.success(function(data){
            sweet.show('', 'Registro modificado', 'success');
            archivoUpload = "";
           // $.unblockUI(); 
            $route.reload();
        })
        obj.error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
        })   
    };
     /******eliminar noticia****/
    $scope.eliminarNoticiaCargar = function(datos){
        console.log(datos);
        $scope.noticia= datos;
        $scope.desabilitado=true;
        $scope.boton="del";
        $scope.titulo="Eliminar Opcion";
    };
     $scope.eliminarNoticia = function(ntcId,datos){
       
        var noticia = {}; 
        noticia['cnt_estado'] = 'B';
        noticia['cnt_id_usuario'] = sessionService.get('IDUSUARIO');
        var parametros = {"table_name":"_ciudadano_noticias", 
                        "body":noticia,
                        "id" : ntcId}; 
        var obj = DreamFactory.api[CONFIG.SERVICE].updateRecord(parametros);
        obj.success(function(data){
            sweet.show('', 'Registro eliminado', 'success');
           // $.unblockUI(); 
            $route.reload();
        })
        obj.error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })   
    };
    /**************************/

    $scope.recuperarDatosPlataformista = function(){
        //Recuperando los datos personales
        var aPersona = {
            table_name:"_bp_personas",
            body:{
              "record": [
                {
                  "prs_id": sIdPltaformista
                }
              ]
            }
        };
        //Servicio
        var obj = DreamFactory.api[CONFIG.SERVICE].getRecordsByPost(aPersona);
        obj.success(function(data){
            if(data.record.length > 0){
                aRegistro.nombre = data.record[0].prs_nombres;
                aRegistro.paterno = data.record[0].prs_paterno;
                aRegistro.materno = data.record[0].prs_materno;
                aRegistro.cedula = data.record[0].prs_ci;
                aRegistro.estado_civil = data.record[0].prs_id_estado_civil;
                aRegistro.sexo = data.record[0].prs_sexo;
                aRegistro.fecha_nacimiento = data.record[0].prs_fec_nacimiento;
                aRegistro.ocupacion = "Plataformista";
                aRegistro.direccion = data.record[0].prs_direccion;
                aRegistro.correo = data.record[0].prs_correo;
                aRegistro.telefono = data.record[0].prs_telefono;
                aRegistro.celular = data.record[0].prs_celular;
            }
        })
        .error(function(data){
            console.log("Error al almacenar registro");
        });
    };
    //datos registro ciudadano 
    $scope.recuperarDatosRegistro = function(){        
            //var sIdRegistro = sessionService.get('IDCIUDADANO');
            var sIdRegistro = 50;
            
            var parametros = {
                "procedure_name":"spmostrardatosperosnales",
                "body":{"params": [{"name":"idPrs","param_type":"IN","value":sIdRegistro}]}
            };            
            DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(parametros).success(function (results){   
            console.log(results[0].vtelefono);          
                if(results.length > 0){
                    aRegistro.nombre = results[0].vnombres;
                    // console.log(aRegistro.nombre);
                    aRegistro.paterno = results[0].paterno;
                    aRegistro.materno = results[0].vmaterno;
                    // console.log(aRegistro.materno);
                    aRegistro.cedula = results[0].vci;
                    aRegistro.estado_civil = results[0].vid_civil;
                    aRegistro.sexo = results[0].vsexo;
                    aRegistro.fecha_nacimiento = results[0].vfecha_na;
                    aRegistro.ocupacion = results[0].vocupacion;
                    aRegistro.direccion = results[0].vdireccion;
                    aRegistro.correo = results[0].vcorreo;
                    aRegistro.telefono = results[0].vtelefono;
                    aRegistro.celular = results[0].vmovil;
                    idEst =  aRegistro.estado_civil;
                    var sIdEstCivil = idEst;
                    var parametros = {
                        "table_name":"_bp_estados_civiles",
                        "body":{                        
                            "filter": "estcivil_id=" + idEst
                        }
                    };            
                    DreamFactory.api[CONFIG.SERVICE].getRecordsByPost(parametros).success(function (results){ 
                      if(results.record.length > 0){              
                        aEstadoC.idEst = results.record[0].estcivil_id;
                        aEstadoC.estado_civil = results.record[0].estcivil;
                      }       
                    }).error(function(results){
                     });   
                }else{
                    $scope.msg = "Error !!";
                
                }             
            }).error(function(results){
            });            
       // }
    };    
    $scope.registro = aRegistro;    
   // console.log($scope.registro);
    //reporte del plataformista
    $scope.aLogPlataforma = "";        
    $scope.datosReportePlaformista = function () {
            var parametros = {
                "procedure_name":"splogciudadano",
                "body":{"params": [{"name":"susuarioid","param_type":"IN","value":sIdPltaformista}]}
            };            
            DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(parametros).success(function (results){       
                if(results.length > 0){
                    $scope.aLogPlataforma = results;
                    //console.log("REPORTE ");
                    //console.log(results);
                }             
            }).error(function(results){
            });                  
    };
    $scope.recuperarNoticias = function(){ 
        var parametros = {
                        "procedure_name":"noticiaslst"
                    };            
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(parametros).success(function (results){ 
                //console.log(results);
                $scope.noticias = results; 
                var data = results;   //grabamos la respuesta para el paginado
                $scope.tablaNoticias = new ngTableParams({
                    page: 1,          
                    count: 4,
                    filter: {},
                    sorting: {}
                }, {
                    total: $scope.noticias.length,
                    getData: function($defer, params) {
                        var filteredData = params.filter() ?
                        $filter('filter')($scope.noticias, params.filter()) :
                        $scope.noticias;              
                        var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        $scope.noticias;
                        params.total(orderedData.length);
                        $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                  
                    }
                });
               
        }).error(function(results){
            alert("error al cargar");
        }); 

    };   
       $scope.registroImagen = aImagen;  
      // console.log($scope.registroImagen);
    //Alerta modificar informacion 
    $scope.modInfoPlataforma = function(){
        sweet.show('Editar Informacion', 'Comuniquese con el administrador', 'error');    
    }
    
    //Iniciando
    $scope.$on('api:ready',function(){
        //$scope.recuperarDatosDocumentos();
        $scope.recuperarDatosPlataformista();        
        $scope.datosReportePlaformista();
        $scope.recuperarNoticias();        
    });   
    $scope.inicioDatosCiudadano = function () {
        if(DreamFactory.isReady()){            
            /*$scope.recuperarDatosRegistro();
            $scope.recuperarDatosDocumentos();
            $.blockUI();*/
            $scope.recuperarDatosPlataformista();            
            $scope.datosReportePlaformista();
            $scope.recuperarNoticias();
        }
    }; 
    $scope.actualizarNoticia = function() {
        $scope.recuperarNoticias();         
    }
    $scope.seleccionarNoticia = function(noticia) {
        $scope.tituloSeleccionado = noticia.cnttitulo;
        $scope.imagenSeleccionada = noticia.cntimagen;
        $scope.resumenSeleccionado = noticia.cntresumen;
        $scope.origenSeleccionado = noticia.cnturl;
    }     
    
    
  
    
    /*
    $scope.recuperarDatosDocumentos = function(){ 
            var sIdRegistro = sessionService.get('IDCIUDADANO');
            var parametros = {
                "procedure_name":"splistadocrecibidos",
                "body":{"params": [{"name":"sidper","param_type":"IN","value":sIdRegistro}]}
            };             
            DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(parametros).success(function (results){
              $scope.DocRecibidos = results; 
              $.unblockUI(); //cerrar la mascara
            }).error(function(results){
                alert("error al cargar");
            }); 
    }; 
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
    }; 
    $scope.registro = aRegistro;
    $scope.registro2 = aEstadoC;

    //reporte del plataformista
    $scope.datosReportePlaformista = function () {
        
        
        
    };
    
     $scope.modificarDatosRegistro = function(vid){
        //$rootScope.vid = sessionService.get('IDCIUDADANO');
        //$location.path("registro_ciudadano|modificarRegistro"); 
        //$scope.recuperarDatosDocumentos();
         //$scope.recuperarDatosPlataformista();
    }  
     $scope.actulizar = function() {
        //$scope.recuperarDatosDocumentos();         

    }*/
});