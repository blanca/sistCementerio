app.controller('cremacionesController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;

    var difuntoID="";
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
        $scope.startDateOpened1 = false;
    };
    $scope.startDateOpen1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened1 = true;
        $scope.startDateOpened = false;
    };
    //listar personas
     $scope.getDifunto = function(){
        $.blockUI();
        var resDifunto = {
            "procedure_name":"fn_lstdifuntos"
        };
        //servicio listar difuntos
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function (response) {
            $scope.obtDifunto = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data es:" +  data);
            $scope.tablaDifunto = new ngTableParams({
                page: 1,          
                count: 5,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtDifunto.length,

                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtDifunto, params.filter()) :
                    $scope.obtDifunto;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtDifunto;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));     
                     console.log($scope.obtDifunto[0].rt_dif_codifun);            
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_difunto"] = error;  
            $.unblockUI();            
        });        
    };
    
    //estados
    /*$scope.getEstados = function(){
        var resEstadoCivil = {
            "procedure_name":"stdcivilst"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoCivil)
        .success(function (response) {
            $scope.estados = response;           
        }).error(function(error) {
            $scope.errors["error_reg_civil"] = error;            
        });
    }*/
    $scope.adicionarDifunto = function(datosDifunto){
        $.blockUI();
        var difunto = {};
        var fechaNacimiento=datosDifunto.fechaNacimiento.getFullYear() + "-" + datosDifunto.fechaNacimiento.getMonth() + "-" + datosDifunto.fechaNacimiento.getDate() + " " + datosDifunto.fechaNacimiento.getHours() + ":" + datosDifunto.fechaNacimiento.getMinutes() + ":" + datosDifunto.fechaNacimiento.getSeconds();
        var resDifunto = {
                    "procedure_name":"sp_insertar_dif",
                    "body":{
                            "params":[                                
                                {
                                    "name": "ci_dif",
                                    "value": datosDifunto.nac_id
                                },                                
                                {
                                    "name": "nombre_dif",
                                    "value":datosDifunto.difNom
                                },
                                {
                                    "name": "paterno_dif",
                                    "value": datosDifunto.difPat
                                },
                                {
                                    "name": "materno_dif",
                                    "value": datosDifunto.difMat
                                },
                                {
                                    "name": "otroApellido_dif",
                                    "value": datosDifunto.difOtroApellido
                                },                                
                                {
                                    "name": "nacionalidad_dif",
                                    "value": datosDifunto.nac_id
                                },                                
                                {
                                    "name": "localidad_dif",
                                    "value": datosDifunto.loc_cod
                                },
                                {
                                    "name": "provincia_dif",
                                    "value":datosDifunto.prov_id
                                },
                                {
                                    "name": "ciudad_dif",
                                    "value": datosDifunto.cuidad
                                },
                                {
                                    "name": "fechaNacimiento_dif",
                                    "value": datosDifunto.fechaNacimiento
                                },
                                {
                                    "name": "sexo_dif",
                                    "value": datosDifunto.difSexo
                                },
                                {
                                    "name": "estadoCivil_dif",
                                    "value": datosDifunto.difEstCivil
                                },
                                {
                                    "name": "profesion_dif",
                                    "value": datosDifunto.difprofesion
                                },
                                {
                                    "name": "observacion_dif",
                                    "value": datosDifunto.difObservacion
                                },
                                {
                                    "name": "gestion_dif",
                                    "value": datosDifunto.difGestion
                                },
                                {
                                    "name": "edad_dif",
                                    "value": datosDifunto.difEdad
                                },
                                {
                                    "name":"usr_id_auditoria",
                                    "value":1
                                },
                                {
                                    "name":"au_tipo_trans_auditoria",
                                    "value":"inhumacion"
                                }
                            ]
                         }
                };
        //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function(data){
           // $scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            $route.reload();
            $scope.getDifuntos();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
            $.unblockUI();
        })
    };

    $scope.modificarDifunto = function(cod_difunto){
        console.log(cod_difunto);
        sessionService.set('ID_DIFUNTO', cod_difunto);
        sessionService.get('ID_DIFUNTO');
       
    };
    $scope.tipopersona= function(tipo){
        var persona ={};
        //$scope.
        if (tipo == 'N' ) {
            persona['tit_ci']=datospersona.rt_tit_ci;
            persona['tit_nombre']=datospersona.rt_tit_nombre;
            persona['tit_ap_pat'] =datospersona.rt_tit_ap_pat;
            persona['tit_mat'] =datospersona.rt_tit_mat;
            persona['tit_tel']=datospersona.rt_tit_tel;
            var resPersona={
                table_name:"fn_lst_natural",
                //id:,
                body:persona
            };
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resPersona);
            obj.success(function(data){
                        
                $.unblockUI(); 
                sweet.show('', 'Registro mostrado', 'success');
                $route.reload();
                //$scope.getDifuntos();
            })
            .error(function(data){
                sweet.show('', 'Registro no mostrado', 'error');
            })

        }
        else
            {
                persona['tit_empresa'] = datospersona.rt_tit_empresa;
                persona['tit_nit'] = datospersona.rt_tit_nit;
                persona['tit_ci'] = datospersona.rt_tit_ci;
                persona['tit_nombre'] = datospersona.rt_tit_nombre;
                persona['tit_ap_pat'] = datospersona.rt_tit_ap_pat;
                persona['tit_mat'] = datospersona.rt_tit_mat;
                persona['tit_tel'] = datospersona.rt_tit_tel;

                var resPersona={
                    table_name:"fn_lst_juridico",
                    body:persona
                }; 
                var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resPersona);
                obj.success(function(data){                            
                    $.unblockUI(); 
                    sweet.show('', 'Registro mostrado', 'success');
                    $route.reload();
                    $scope.getDifuntos();
                })
                .error(function(data){
                    sweet.show('', 'Registro no mostrado', 'error');
                })
             }
};
    //////////////////////
    $scope.seleccionarDifunto = function(rtci){
        //var difunto ={};
       // difunto

    };
    //////////////////////
    $scope.eliminarPersona = function(prsId){
        $.blockUI();
        var persona = {};
        persona['prs_modificado'] = fechactual;
        persona['prs_estado'] = 'B';
        persona['prs_usr_id'] = sessionService.get('IDUSUARIO');

        var resPersona = {
            table_name:"_bp_personas",
            id:prsId,
            body:persona
        };
        //servicio eliminar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resPersona);
        obj.success(function(data){
            $scope.getEstados();
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
    $scope.modificarPersonaCargar = function(persona){
        $scope.desabilitado=false;
        $scope.datosPersona = persona;
        $scope.boton="upd";
        $scope.titulo="Modificar Personas";
    };
    $scope.eliminarPersonaCargar = function(persona){
        $scope.desabilitado=true;
        $scope.datosPersona = persona;
        $scope.boton="del";
        $scope.titulo="Eliminar Personas";
    };
    $scope.limpiar = function(){
        $scope.datosPersona='';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Personas";
    };

    $scope.crearDifunto = function(){
        $scope.nacionalidad();
        $scope.localidad();
        $scope.provincia();
        $scope.profesion();

    };
//mi codigo para los conbos---nacionalidad---
    $scope.nacionalidad = function(){
        var resNacionalidad = {
            "procedure_name":"fn_lst_nacionalidad"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resNacionalidad)
        .success(function (response) {
            $scope.nacionalidad = response;  
            console.log("esta es la respuesta:" + response);         
        }).error(function(error) {
            $scope.errors["error_reg_nacionalidad"] = error;            
        });
    };
    //fin nacionalidad
    //localidad
    $scope.localidad = function(){
        var reslocalidad = {
           "procedure_name":"fn_lst_localidad"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(reslocalidad)
        .success(function (response){
            $scope.localidad = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_localidad"]=error;
        });

    };
    //fin
    //provincia
    $scope.provincia = function(){
        var resprovincia = {
            "procedure_name":"fn_lst_provincia"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprovincia)
        .success(function (response){
            $scope.provincia = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_provincia"]=error;
        });

    };
    //fin
    //profesion
    $scope.profesion = function(){
        var resprofesion = {
            "procedure_name":"fn_lst_profesion"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprofesion)
        .success(function (response){
            $scope.profesion = response;
        })
        .error(function(error){
            $scope.errors["error_reg_profesion"]=error;

        });
    };
    //fin





    $scope.$on('api:ready',function(){
        $scope.getDifunto();
        //$scope.getEstados();
    });
    $scope.inicioCremaciones = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.getDifunto();
           // $scope.getEstados();
        }
    };    
    
});
