app.controller('difuntoController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet,MyService) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
	var codigo_difunto = 0;
	$scope.boton_cert="new";
	$scope.boton="new";
	
	$scope.cod_tit = MyService.data.tit_codtit_t;
   $scope.nombre = MyService.data.tit_nombre_t;
   $scope.paterno = MyService.data.tit_ap_pat_t;
   $scope.materno = MyService.data.tit_mat_t;
   $scope.carnet = MyService.data.tit_ci_t;
   $scope.avenida = MyService.data.tit_via_t;
   $scope.nro = MyService.data.tit_vianum_t;
   $scope.zona = MyService.data.tit_zona_t;
   
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
        $scope.startDateOpened1 = false;
    };
    $scope.startDateOpen1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened1 = true;
        $scope.startDateOpened = false;
    };
    //listar personas  hhhhhhh
     $scope.getDifunto = function(codi){
        $.blockUI();
        var resDifunto = {
            "procedure_name":"fn_lstdifuntos_uno",
			"body":{"params": [{"name":"codDifunto","param_type":"IN","value":1}]}
        };
        //servicio listar difuntos
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function (response) {
            $scope.obtDifunto = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data es:" +  data);
            $scope.tablaDifunto = new ngTableParams({
                page: 1,          
                count: 10,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtDifunto.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtDifunto, params.filter()) :
                    $scope.obtDifunto;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtDifunto;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_difunto"] = error;  
            $.unblockUI();            
        });        
    };
    //buscar item recaudador
	//ITEM POR DEFECTO PARA INHUMACION ES IGUA A 1
	$scope.getItemRecaudacion = function(){
        $.blockUI();
        var resItemRecaudador = {
            "procedure_name":"fn_lst_itemrecaudador",
			"body":{"params": [{"name":"codigo","param_type":"IN","value":1}]}
        };
        //listar recaudador
		var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resItemRecaudador);
        obj.success(function (response) {
            
            var data_item = response;   //grabamos la respuesta para el paginado
			$scope.codigo_item = data_item[0].itemrcal_r;
			$scope.descripcion_item = data_item[0].itemrdes_r;
			$scope.importe = data_item[0].itemrmon_r;
            console.log("el data es itemrecaudador:" + data_item);
			$.unblockUI();
          })
        obj.error(function(error) {
            $scope.errors["error_titular"] = error;  
            $.unblockUI();            
        });        
    };
    
    //estados
    /*$scope.getEstados = function(){
        var resEstadoCivil = {
            "procedure_name":"stdcivilst"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoCivil)
        .success(function (response) {
            $scope.estados = response;           
        }).error(function(error) {
            $scope.errors["error_reg_civil"] = error;            
        });
    }*/
    $scope.adicionarDifunto = function(datosDifunto){
        $.blockUI();
        var difunto = {};
        var fechaNacimiento=datosDifunto.fechaNacimiento.getFullYear() + "-" + datosDifunto.fechaNacimiento.getMonth() + "-" + datosDifunto.fechaNacimiento.getDate() + " " + datosDifunto.fechaNacimiento.getHours() + ":" + datosDifunto.fechaNacimiento.getMinutes() + ":" + datosDifunto.fechaNacimiento.getSeconds();
        var resDifunto = {
                    "procedure_name":"sp_insertar_dif_ul",
                    "body":{
                            "params":[                                
                                {
                                    "name": "ci_dif",
                                    "value": datosDifunto.difCi
                                },                                
                                {
                                    "name": "nombre_dif",
                                    "value":datosDifunto.difNom
                                },
                                {
                                    "name": "paterno_dif",
                                    "value": datosDifunto.difPat
                                },
                                {
                                    "name": "materno_dif",
                                    "value": datosDifunto.difMat
                                },
                                {
                                    "name": "otroApellido_dif",
                                    "value": datosDifunto.difOtroApellido
                                },                                
                                {
                                    "name": "nacionalidad_dif",
                                    "value": datosDifunto.nac_id
                                },                                
                                {
                                    "name": "localidad_dif",
                                    "value": datosDifunto.loc_cod
                                },
                                {
                                    "name": "provincia_dif",
                                    "value":datosDifunto.prov_id
                                },
                                {
                                    "name": "ciudad_dif",
                                    "value": datosDifunto.cuidad
                                },
                                {
                                    "name": "fechaNacimiento_dif",
                                    "value": datosDifunto.fechaNacimiento
                                },
                                {
                                    "name": "sexo_dif",
                                    "value": datosDifunto.difSexo
                                },
                                {
                                    "name": "estadoCivil_dif",
                                    "value": datosDifunto.difEstCivil
                                },
                                {
                                    "name": "profesion_dif",
                                    "value": datosDifunto.difprofesion
                                },
                                {
                                    "name": "observacion_dif",
                                    "value": datosDifunto.difObservacion
                                },
                                {
                                    "name": "gestion_dif",
                                    "value": datosDifunto.difGestion
                                },
                                {
                                    "name": "edad_dif",
                                    "value": datosDifunto.difEdad
                                }/*,
                                {
                                    "name":"usr_id_auditoria",
                                    "value":1
                                },
                                {
                                    "name":"au_tipo_trans_auditoria",
                                    "value":"inhumacion"
                                }*/
                            ]
                         }
                };
        //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function(data){
           // $scope.getEstados();
            $scope.codigo_difunto=data[0].ultimo;
			
             
            sweet.show('', 'Registro insertado' + $scope.codigo_difunto , 'success');
            /*$route.reload();
            $scope.getDifuntos();*/
			$.unblockUI();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertadooo' , 'error');
            $.unblockUI();
        })
		///////////////////////////////
		/*var relacion = {}; 
        relacion['tid_titular'] = MyService.data.tit_codtit_t;
        relacion['tid_difunto'] = $scope.codigo_difunto;
        relacion['tid_proceso'] = 1;
        relacion['tid_fecha_modificado'] = fechactual;
        relacion['tid_estado'] = 'A';
         var resRelacion = {
            table_name:"_bp_usuarios",
            body:usuario
        };
		///servicio insertar usuarios
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resRelacion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })*/
		///////////////////////////////
		$scope.boton="upd";
    };

   $scope.modificarDifunto = function(datosDifunto){
        $.blockUI();

        var difunto = {};
        
        difunto['dif_nombre'] = datosDifunto.difNom;
        difunto['dif_ap_pat'] = datosDifunto.difPat;
        difunto['dif_ap_mat'] = datosDifunto.difMat;
        difunto['dif_otro_ap'] = datosDifunto.difOtroApellido;
        difunto['dif_ci'] = datosDifunto.difCi;
        difunto['nac_id'] = datosDifunto.nac_id;
        difunto['prov_id'] = datosDifunto.prov_id;
        difunto['loc_cod'] = datosDifunto.loc_cod;
        difunto['dif_oriciu'] = datosDifunto.ciudad;
        difunto['dif_fechanac'] = datosDifunto.fechaNacimiento;
        difunto['dif_sexo'] = datosDifunto.difSexo;
        difunto['dif_est_civil'] = datosDifunto.difEstCivil;
        difunto['prof_id'] = datosDifunto.difprofesion;
        difunto['dif_observacion'] = datosDifunto.difObservacion;
        difunto['dif_gestion'] = datosDifunto.difGestion;
        difunto['dif_edad'] = datosDifunto.difEdad;
        //difunto['dif_codifun'] = sessionService.get('IDUSUARIO');

        var resDifunto = {
            table_name:"sat_difunto",
            id:$scope.codigo_difunto,
            body:difunto
        };
        //servicio modificar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resDifunto);
        obj.success(function(data){
            //$scope.getEstados();
           
            sweet.show('', 'Registro modificado', 'success');
            $.unblockUI(); 
		    //$route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
			$.unblockUI();
        })
    };
    $scope.eliminarDifunto = function(rt_dif_codifun){
        $.blockUI();
        var difunto = {};
        difunto['dif_modificado'] = fechactual;
        difunto['dif_estado'] = 'INACTIVO';
        difunto['n_trans_id'] = sessionService.get('IDUSUARIO');

        var resDifunto = {
            table_name:"sat_difunto",
            id:rt_dif_codifun,
            body:difunto
        };
        //servicio eliminar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resDifunto);
        obj.success(function(data){
            //$scope.getEstados();
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
	
	///////
	$scope.adicionarCertificado = function(datosCertificado){
        $.blockUI();
        var certificado = {};
        //var fechaNacimiento=datosPersona.prsfecnmt.getFullYear() + "-" + datosPersona.prsfecnmt.getMonth() + "-" + datosPersona.prsfecnmt.getDate() + " " + datosPersona.prsfecnmt.getHours() + ":" + datosPersona.prsfecnmt.getMinutes() + ":" + datosPersona.prsfecnmt.getSeconds();
		var resCertificado = {
                    "procedure_name":"sp_insertar_certificado",
                    "body":{
                            "params":[                                
                                {
                                    "name": "inh_codifun_c",
                                    "value": 1
                                },
								{
                                    "name": "inh_codmac_c",
                                    "value":1
                                },
								{
                                    "name": "inh_coddis_c",
                                    "value":1
                                }, 
								{
                                    "name": "inh_codcm_c",
                                    "value": datosCertificado.rt_codcm
                                }, 
								{
                                    "name": "inh_nro_reg_civil_c",
                                    "value":datosCertificado.rt_nroregciv
                                }, 
								{
                                    "name": "n_trans_id_c",
                                    "value":1
                                }, 
								{
                                    "name": "inh_oficial_reg_civil_c",
                                    "value":datosCertificado.rt_oficialregciv
                                }, 
								{
                                    "name": "inh_libro_c",
                                    "value":datosCertificado.rt_libro
                                }, 
								{
                                    "name": "inh_folio_c",
                                    "value":datosCertificado.rt_folio
                                }, 
								{
                                    "name": "inh_partida_c",
                                    "value":datosCertificado.rt_partida
                                }, 
								{
                                    "name": "inh_fec_fallecimiento_c",
                                    "value":datosCertificado.rt_fecfallecimiento
                                }, 
								{
                                    "name": "inh_hra_fallecimiento_c",
                                    "value":datosCertificado.rt_hrafallecimiento
                                }, 
								{
                                    "name": "inh_nombre_medico_certificado_defun_c",
                                    "value":datosCertificado.rt_medicodefun
                                }, 
								{
                                    "name": "inh_departamento_falle_c",
                                    "value":datosCertificado.rt_deptofalle
                                },
								{
                                    "name": "prov_id_c",
                                    "value":datosCertificado.rt_provid
                                },
								{
                                    "name": "loc_cod_c",
                                    "value":datosCertificado.rt_loccod
                                },
								{
                                    "name": "inh_ciudad_falle_c",
                                    "value":datosCertificado.rt_ciudadfalle
                                },
								{
                                    "name": "inh_fchareg_c",
                                    "value":datosCertificado.rt_fchareg
                                },
								{
                                    "name": "inh_fcharegistrado_c",
                                    "value":fechactual
                                }
							  ]
                         }
                };
/*
        certificado['inh_oficial_reg_civil'] = datosCertificado.rt_oficialregciv;
        certificado['inh_fchareg'] = datosCertificado.rt_fchareg;
        certificado['inh_nro_reg_civil'] = datosCertificado.rt_nroregciv;
        certificado['inh_libro'] = datosCertificado.rt_libro;
        certificado['inh_partida'] = datosCertificado.rt_partida;
        certificado['inh_folio'] = datosCertificado.rt_folio;
        certificado['inh_fec_fallecimiento'] = datosCertificado.rt_fecfallecimiento;
        certificado['inh_hra_fallecimiento'] = datosCertificado.rt_hrafallecimiento;

        certificado['inh_ciudad_falle'] = datosCertificado.rt_ciudadfalle;
        certificado['inh_departamento_falle'] = datosCertificado.rt_deptofalle;
        certificado['prov_id'] = datosCertificado.rt_provid;
        certificado['loc_cod'] = datosCertificado.rt_loccod;

        certificado['inh_nombre_medico_certificado_defun'] = datosCertificado.rt_medicodefun;
        certificado['inh_codcm'] = datosCertificado.rt_codcm;

        certificado['inh_codifun'] = '1';
        certificado['inh_codmac'] = '1';
        certificado['inh_coddis'] = '1';

        certificado['n_trans_id'] = '1';

        certificado['inh_fcharegistrado'] = fechactual;
        certificado['inh_modificado'] = fechactual;
        certificado['inh_estado'] = 'ACTIVO';
        
        var resCertificado = {
            table_name:"sat_certificadoinhumacion",
            body:certificado
        };*/
        //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resCertificado);
        obj.success(function(data_c){    
            $scope.codigo_certificado=data_c[0].ultimo;
            sweet.show('', 'Registro insertado', 'success');
			 $.unblockUI();
            //$route.reload();
            //$scope.getCertificado();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
            $.unblockUI();
        })
		$scope.boton_cert="upd";
    };
	//////////////////////
	$scope.modificarCertificado = function(datosCertificado){
        $.blockUI();
        var certificado = {};

        certificado['inh_oficial_reg_civil'] = datosCertificado.rt_oficialregciv;
        certificado['inh_fchareg'] = datosCertificado.rt_fchareg;
        certificado['inh_nro_reg_civil'] = datosCertificado.rt_nroregciv;
        certificado['inh_libro'] = datosCertificado.rt_libro;
        certificado['inh_partida'] = datosCertificado.rt_partida;
        certificado['inh_folio'] = datosCertificado.rt_folio;
        certificado['inh_fec_fallecimiento'] = datosCertificado.rt_fecfallecimiento;
        certificado['inh_hra_fallecimiento'] = datosCertificado.rt_hrafallecimiento;

        certificado['inh_ciudad_falle'] = datosCertificado.rt_ciudadfalle;
        certificado['inh_departamento_falle'] = datosCertificado.rt_deptofalle;
        certificado['prov_id'] = datosCertificado.rt_provid;
        certificado['loc_cod'] = datosCertificado.rt_loccod;

        certificado['inh_codcm'] = datosCertificado.rt_codcm;
        certificado['inh_nombre_medico_certificado_defun'] = datosCertificado.rt_medicodefun;

        certificado['inh_codifun'] = '1';
        certificado['inh_codmac'] = '1';
        certificado['inh_coddis'] = '1';

        certificado['n_trans_id'] = '1';

        certificado['inh_modificado'] = fechactual;

        var resCertificado = {
            table_name:"sat_certificadoinhumacion",
            id:$scope.codigo_certificado,
            body:certificado
        };
        //servicio modificar 
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resCertificado);
        obj.success(function(data){
            //$scope.getEstados();
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            //$route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
           
        })
    };
	/////////////////////////////
	
    $scope.modificarDifuntoCargar = function(difunto){
        $scope.desabilitado=false;
        $scope.datosDifunto = difunto;
        $scope.boton="upd";
        $scope.titulo="Modificar Difuntos";

    };
    $scope.eliminarDifuntoCargar = function(difunto){
        $scope.desabilitado=true;
        $scope.datosDifunto = difunto;
        $scope.boton="del";
        $scope.titulo="Eliminar Difuntos";
    };
    $scope.limpiar = function(){
        $scope.datosDifunto='';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Difuntos";
    };

    $scope.crearDifunto = function(){
        $scope.datosDifunto='';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Difuntos";
        $scope.nacionalidad();
        $scope.localidad();
        $scope.provincia();
        $scope.profesion();

    };
//mi codigo para los conbos---nacionalidad---
    $scope.nacionalidad = function(){
        var resNacionalidad = {
            "procedure_name":"fn_lst_nacionalidad"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resNacionalidad)
        .success(function (response) {
            $scope.nacionalidad = response;  
            console.log("esta es la respuesta:" + response);         
        }).error(function(error) {
            $scope.errors["error_reg_nacionalidad"] = error;            
        });
    };
    //fin nacionalidad
    //localidad
    $scope.localidad = function(){
        var reslocalidad = {
           "procedure_name":"fn_lst_localidad"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(reslocalidad)
        .success(function (response){
            $scope.localidad = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_localidad"]=error;
        });

    };
    //fin
    //provincia
    $scope.provincia = function(){
        var resprovincia = {
            "procedure_name":"fn_lst_provincia"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprovincia)
        .success(function (response){
            $scope.provincia = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_provincia"]=error;
        });

    };
    //fin
    //profesion
    $scope.profesion = function(){
        var resprofesion = {
            "procedure_name":"fn_lst_profesion"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprofesion)
        .success(function (response){
            $scope.profesion = response;
        })
        .error(function(error){
            $scope.errors["error_reg_profesion"]=error;

        });
    };
    //fin
	//Combo Causa Muerte
    $scope.muerte = function(){
        var resMuerte = {
            "procedure_name":"fn_lst_causamuerte"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resMuerte)
        .success(function (response) {
            $scope.muerte = response;  
            console.log("esta es la respuesta:" + response);         
        }).error(function(error) {
            $scope.errors["error_reg_muerte"] = error;            
        });
    };


    $scope.$on('api:ready',function(){
        $scope.getDifunto(0);
        //$scope.getEstados();
        $scope.nacionalidad();
        $scope.localidad();
        $scope.provincia();
        $scope.profesion();
		$scope.muerte();

    });
    $scope.inicioDifunto = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
        $scope.getDifunto(0);
           // $scope.getEstados();
        $scope.nacionalidad();
        $scope.localidad();
        $scope.provincia();
        $scope.profesion();
		$scope.muerte();

        }
    };    
    
});
