app.controller('exhumacionController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet,$routeParams,MyService,$location) {
											     
	var fecha= new Date();
	$scope.cod_tit = MyService.data.tit_codtit_t;
   $scope.nombre = MyService.data.tit_nombre_t;
   $scope.paterno = MyService.data.tit_ap_pat_t;
   $scope.materno = MyService.data.tit_mat_t;
   $scope.carnet = MyService.data.tit_ci_t;
   
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
        $scope.startDateOpened1 = false;
    };
    $scope.startDateOpen1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened1 = true;
        $scope.startDateOpened = false;
	};
   console.log($scope.nombre);

//listar personas
     $scope.listarDifunto = function(){
        $.blockUI();
        var resDifunto = {
            "procedure_name":"fn_lstdifuntos"
        };
        //servicio listar difuntos
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function (response) {
            $scope.obtDifunto = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data es EX:" +  data);
            $scope.tablaDifunto = new ngTableParams({
                page: 1,          
                count: 5,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtDifunto.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtDifunto, params.filter()) :
                    $scope.obtDifunto;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtDifunto;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_difunto"] = error;  
            $.unblockUI();            
        });        
    };
	//+++++++++++++++++++++++++++++++
	   //buscar difuntu
  $scope.buscarDifunto = function(modelo_difunto){
   $scope.prueba="holasss";
   $scope.listarDifunto();
   };
   
   $scope.optenerSeleccion = function(difunto){
	   console.log(difunto);
	   $scope.seleccion = difunto;
	   $scope.codigo_difunto = difunto.rt_dif_codifun;
	   };
   
	 //**************************** 
	 $scope.adicionarTranslado = function(translado){
		$.blockUI();
        var objeto = {};
        var fechatranslado=translado.fecha_tra.getFullYear() + "-" + translado.fecha_tra.getMonth() + "-" + translado.fecha_tra.getDate(); 
		var fecha= new Date();
        var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();

		 objeto['tit_codtit'] = translado.cod_titular;
		 objeto['dif_codifun'] = $scope.codigo_difunto;
		 objeto['tra_fecha_translado'] = fechatranslado;
		 objeto['tra_fecha_registro'] = fechactual; 
		 objeto['tra_motivo'] = translado.motivo;
		  
	     console.log(translado.cod_titular);
		 console.log($scope.codigo_difunto);
		 console.log(fechatranslado);
		 console.log(fechactual);
		 console.log(translado.motivo);
	
		 
		var resTranslado = {
            table_name:"sat_translado",
            body:objeto
        
		};
		 //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resTranslado);
        obj.success(function(data){
            //$scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
			$location.url("satelite|titular|index.html");;
            //$route.reload();
            //s$scope.getPersonas();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })
	}; 
   ////*****************************
	 $scope.$on('api:ready',function(){
       // $scope.getTitular();
	 //	$scope.nacionalidad();
        //$scope.getEstados();
    });
    $scope.inicioEntierro= function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
          //  $scope.getTitular();
		//	$scope.nacionalidad();
           // $scope.getEstados();
        }
    };

});