app.controller('nacController', function ($scope,$location,$route,CONFIG,DreamFactory,sessionService,ngTableParams,$filter,sweet) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
      //listar roles
    $scope.getNacion = function(){
        $.blockUI();
        var resNacion = {
            "procedure_name":"fn_lst_nacionalidad"
        };
        //servicio listar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resNacion);
        obj.success(function (response) {
            $scope.obtNacion = response;
            var data = response;   //grabamos la respuesta para el paginado
            $scope.tablaNacion = new ngTableParams({
                page: 1,          
                count: 10,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtNacion.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtNacion, params.filter()) :
                    $scope.obtNacion;              
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.obtNacion;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            }); 
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_nacion"] = error;            
        });        
    };
    $scope.adicionarNacion = function(datosNacion){
        $.blockUI();
        var nacion = {};
        nacion['nac_descripcion'] = datosNacion.fn_nac_descripcion;
        nacion['nac_fcharegistrado'] = fechactual;
        nacion['nac_modificado'] = fechactual;
        nacion['nac_id'] = sessionService.get('IDUSUARIO');
        nacion['nac_estado'] = 'S';
        var resNacion = {
            table_name:"sat_nacionalidad",
            body:nacion
        };
        //servicio insertar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resNacion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            //handleRoles($scope.conexion.getListSP('roleslst',{}), 'post');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })
    };

    $scope.modificarNacion = function(fn_nac_id,datosNacion){
        $.blockUI();
        var nacion = {};
        nacion['nac_descripcion'] = datosNacion.fn_nac_descripcion;
        nacion['nac_modificado'] = fechactual;
        nacion['nac_id'] = sessionService.get('IDUSUARIO');
        //handleRoles($scope.conexion.update(rolId,rol), 'update');
        var resNacion = {
            table_name:"sat_nacionalidad",
            id:fn_nac_id,
            body:nacion
        };
        //servicio modificar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resNacion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            //handleRoles($scope.conexion.getListSP('roleslst',{}), 'post');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
        })
        //$route.reload();
    };
    $scope.eliminarNacion = function(fn_nac_id){
        $.blockUI();
        var nacion = {};
        nacion['nac_modificado'] = fechactual;
        nacion['nac_estado'] = 'I';
        nacion['nac_id'] = sessionService.get('IDUSUARIO');
         var resNacion = {
            table_name:"sat_nacionalidad",
            id:fn_nac_id,
            body:nacion
        };
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resNacion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
    $scope.modificarNacionCargar = function(nacion){
        $scope.only=false;
        $scope.datosNacion=nacion;
        $scope.boton="upd";
        $scope.titulo="Modificar Nacionalidad";
    };
    $scope.eliminarNacionCargar = function(nacion){
        $scope.only=true;
        $scope.datosNacion=nacion;
        $scope.boton="del";
        $scope.titulo="Eliminar nacionalidad";
    };
    $scope.limpiar = function(){
        $scope.only=false;
        $scope.datosNacion = '';
        $scope.boton="new";
        $scope.titulo="Registro de Nacionalidad";
    }; 
    $scope.$on('api:ready',function(){
        $scope.getNacion();       
    });
    $scope.inicioNacion = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.getNacion();
            
        }
    }; 

});