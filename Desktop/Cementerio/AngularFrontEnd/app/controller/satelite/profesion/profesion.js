app.controller('profController', function ($scope,$location,$route,CONFIG,DreamFactory,sessionService,ngTableParams,$filter,sweet) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
      //listar roles
    $scope.getProfesion = function(){
        $.blockUI();
        var resProfesion = {
            "procedure_name":"fn_lst_profesion"
        };
        //servicio listar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resProfesion);
        obj.success(function (response) {
            $scope.obtProfesion = response;
            var data = response;   //grabamos la respuesta para el paginado
            $scope.tablaProfesion = new ngTableParams({
                page: 1,          
                count: 10,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtProfesion.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtProfesion, params.filter()) :
                    $scope.obtProfesion;              
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.obtProfesion;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            }); 
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_profesion"] = error;            
        });        
    };
    $scope.adicionarProfesion = function(datosProfesion){
        $.blockUI();
        var profesion = {};
        profesion['prof_desc'] = datosProfesion.fn_prof_descripcion;
        profesion['prof_fcharegistrado'] = fechactual;
        profesion['prof_modificado'] = fechactual;
        profesion['prof_id'] = sessionService.get('IDUSUARIO');
        profesion['prof_estado'] = 'ACTIVO';
        var resProfesion = {
            table_name:"sat_profesion",
            body:profesion
        };
        //servicio insertar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resProfesion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            //handleRoles($scope.conexion.getListSP('roleslst',{}), 'post');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })
    };

    $scope.modificarProfesion = function(fn_prof_id,datosProfesion){
        $.blockUI();
        var profesion = {};
        profesion['prof_desc'] = datosProfesion.fn_prof_descripcion;
        profesion['prof_modificado'] = fechactual;
        profesion['prof_id'] = sessionService.get('IDUSUARIO');
        //handleRoles($scope.conexion.update(rolId,rol), 'update');
        var resProfesion = {
            table_name:"sat_profesion",
            id:fn_prof_id,
            body:profesion
        };
        //servicio modificar roles
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resProfesion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            //handleRoles($scope.conexion.getListSP('roleslst',{}), 'post');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
        })
        //$route.reload();
    };
    $scope.eliminarProfesion = function(fn_prof_id){
        $.blockUI();
        var profesion = {};
        profesion['prof_modificado'] = fechactual;
        profesion['prof_estado'] = 'I';
        profesion['prof_id'] = sessionService.get('IDUSUARIO');
         var resProfesion = {
            table_name:"sat_profesion",
            id:fn_prof_id,
            body:profesion
        };
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resProfesion);
        obj.success(function(data){
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
    $scope.modificarProfesionCargar = function(profesion){
        $scope.only=false;
        $scope.datosProfesion=profesion;
        $scope.boton="upd";
        $scope.titulo="Modificar Profesion";
    };
    $scope.eliminarProfesionCargar = function(profesion){
        $scope.only=true;
        $scope.datosProfesion=profesion;
        $scope.boton="del";
        $scope.titulo="Eliminar Profesion";
    };
    $scope.limpiar = function(){
        $scope.only=false;
        $scope.datosProfesion = '';
        $scope.boton="new";
        $scope.titulo="Registro de Profesion";
    }; 
    $scope.$on('api:ready',function(){
        $scope.getProfesion();       
    });
    $scope.inicioProfesion = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.getProfesion();
            
        }
    }; 

});