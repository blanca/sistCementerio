app.controller('titularController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet,$location,MyService) {
	
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
        $scope.startDateOpened1 = false;
    };
    $scope.startDateOpen1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened1 = true;
        $scope.startDateOpened = false;
    };
    //listar personas nuevas 
     $scope.getTitular = function(){
        $.blockUI();
        var resTitular = {
            "procedure_name":"fn_lsttitulares"
        };
        //servicio listar titulares
		var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resTitular);
        obj.success(function (response) {
            $scope.obtTitular = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data ess:" +  data.length);
            $scope.tablaTitular = new ngTableParams({
                page: 1,          
                count: 5,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtTitular.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtTitular, params.filter()) :
                    $scope.obtTitular;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtTitular;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_titular"] = error;  
            $.unblockUI();            
        });        
    };
    
    //estados
    /*$scope.getEstados = function(){
        var resEstadoCivil = {
            "procedure_name":"stdcivilst"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoCivil)
        .success(function (response) {
            $scope.estados = response;           
        }).error(function(error) {
            $scope.errors["error_reg_civil"] = error;            
        });
    }*/


  
    $scope.adicionarTitular = function(datosTitular){
        $.blockUI();
        var Titular = {};
        var fechaNacimiento=datosTitular.tit_fchnac_t.getFullYear() + "-" + datosTitular.tit_fchnac_t.getMonth() + "-" + datosTitular.tit_fchnac_t.getDate() + " " + datosTitular.tit_fchnac_t.getHours() + ":" + datosTitular.tit_fchnac_t.getMinutes() + ":" + datosTitular.tit_fchnac_t.getSeconds();
        var resTitular = {
                    "procedure_name":"sp_insertar_tit",
                    "body":{
                            "params":[
							     {
                                    "name":"tipopersona_tit",
                                    "value":"Juridica"
                                },
								{
                                    "name":"idrepresentante_tit",
                                    "value":1
                                }, 
								{
                                    "name":"transaccion_tit",
                                    "value":1
                                },                               
                                {
                                    "name": "ci_tit",
                                    "value": datosTitular.tit_ci_t
                                },                                
                                {
                                    "name": "nombre_tit",
                                    "value":datosTitular.tit_nombre_t
                                },
                                {
                                    "name": "paterno_tit",
                                    "value": datosTitular.tit_ap_pat_t
                                },
                                {
                                    "name": "materno_tit",
                                    "value": datosTitular.tit_mat_t
                                },
                                {
                                    "name": "otroApellido_tit",
                                    "value": datosTitular.tit_otro_ap_t
                                },                                
                                {
                                    "name": "nacionalidad_tit",
                                    "value": datosTitular.tit_nac_t
                                },                                
                                {
                                    "name": "ciudad_tit",
                                    "value": datosTitular.tit_ciciu_t
                                },
                                {
                                    "name": "fechaNacimiento_tit",
                                    "value": datosTitular.tit_fchnac_t
                                },
                                {
                                    "name": "sexo_tit",
                                    "value": datosTitular.tit_sexo_t
                                },
                                {
                                    "name": "estadoCivil_tit",
                                    "value": datosTitular.tit_civ_t
                                },
                            	{
                                    "name":"telefono_tit",
                                    "value":datosTitular.tit_tel_t
                                },
								{
                                    "name":"casilla_tit",
                                    "value":datosTitular.tit_cas_t
                                },
								{
                                    "name":"fax_tit",
                                    "value":datosTitular.tit_fax_t
                                },
								{
                                    "name":"email_tit",
                                    "value":datosTitular.tit_email_t
                                },
								{
                                    "name":"direccion_tit",
                                    "value":datosTitular.tit_via_t
                                },
								{
                                    "name":"nroDireccion_tit",
                                    "value":datosTitular.tit_vianum_t
                                },
								{
                                    "name":"zona_tit",
                                    "value":datosTitular.tit_zona_t
                                },
								{
                                    "name":"representante_tit",
                                    "value":datosTitular.tit_rep_legal_t
                                },
								{
                                    "name":"libreta_tit",
                                    "value":datosTitular.tit_lib_t
                                }
                            ]
                         }
                };
        //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resTitular);
        obj.success(function(data){
           // $scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro insertado' , 'success');
            $route.reload();
            $scope.getDifuntos();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
            $.unblockUI();
        })
    };
	  $scope.modificarTitular = function(tit_id, datosTitular){
        $.blockUI();
        var titular = {};
		titular['tit_codtit_tipopersona'] = "Natural";
		titular['tit_fumrep_leg_id'] = 1;
		titular['n_trans_id'] = 1;
		titular['tit_ci'] = datosTitular.tit_ci;
		titular['tit_nombre'] = datosTitular.tit_nombre_t;
		titular['tit_ap_pat'] = datosTitular.tit_ap_pat_t;
		titular['tit_mat'] = datosTitular.tit_mat_t;
		titular['tit_otro_ap'] = datosTitular.tit_otro_ap_t;
		titular['tit_nac'] = datosTitular.tit_nac_t;
		titular['tit_ciciu'] = datosTitular.tit_ciciu_t;
		titular['tit_fchnac'] = datosTitular.tit_fchnac_t;
		titular['tit_sexo'] = datosTitular.tit_sexo_t;
		titular['tit_civ'] = datosTitular.tit_civ_t;
		titular['tit_tel'] = datosTitular.tit_tel_t;
		titular['tit_cas'] = datosTitular.tit_cas_t;
		titular['tit_fax'] = datosTitular.tit_fax_t;
		titular['tit_email'] = datosTitular.tit_email_t;
		titular['tit_via'] = datosTitular.tit_via_t;
		titular['tit_vianum'] = datosTitular.tit_vianum_t;
		titular['tit_zona'] = datosTitular.tit_zona_t;
		titular['tit_rep_legal'] = datosTitular.tit_rep_legal_t;
		titular['tit_lib'] = datosTitular.tit_lib_t;
		
		var resTitular = {
            table_name:"sat_titular",
            id:tit_id,
            body:titular
        };
	  //servicio modificar titular
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resTitular);
        obj.success(function(data){
            /*$scope.getEstados();*/
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
        })
    };
	
	/*
    $scope.eliminarPersona = function(prsId){
        $.blockUI();
        var persona = {};
        persona['prs_modificado'] = fechactual;
        persona['prs_estado'] = 'B';
        persona['prs_usr_id'] = sessionService.get('IDUSUARIO');

        var resPersona = {
            table_name:"_bp_personas",
            id:prsId,
            body:persona
        };
        //servicio eliminar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resPersona);
        obj.success(function(data){
            $scope.getEstados();
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })
    }; 
    
    $scope.eliminarPersonaCargar = function(persona){
        $scope.desabilitado=true;
        $scope.datosPersona = persona;
        $scope.boton="del";
        $scope.titulo="Eliminar Personas";
    };
    $scope.limpiar = function(){
        $scope.datosPersona='';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Personas";
    };
*/
	$scope.modificarTitularCargar = function(titular){
       /* $scope.nacionalidad();*/
		$scope.desabilitado=false;
        $scope.datosTitular = titular;
        $scope.boton="upd";
        $scope.titulo="Modificar Titularess";
    };
    $scope.crearTitular = function(){
        $scope.datosTitular='';
		
		$scope.desabilitado=false;
		$scope.titulo="Registro de Titulares";
        $scope.boton="new";
    };
	$scope.inhumacionMayor = function(titular){
		MyService.data = titular;
		$location.url("satelite|difunto|index.html");
	};
	$scope.inhumacionMenor = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.exhumacion = function(titular){
		MyService.data = titular;
		$location.url("satelite|exhumacion|index.html");
	};
	$scope.reubicacion = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.renovacion = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.translado = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.obito = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.obito = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	$scope.autorizacion = function(titular){
		MyService.data = titular;
		$location.url("satelite|entierro|index.html");
	};
	

//mi codigo para los conbos--- ---
    $scope.nacionalidad = function(){
        var resNacionalidad = {
            "procedure_name":"fn_lst_nacionalidad"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resNacionalidad)
        .success(function (response) {
            $scope.nacionalidad = response;  
            console.log("esta es la respuesta:" + response);         
        }).error(function(error) {
            $scope.errors["error_reg_nacionalidad"] = error;            
        });
    };
    //fin nacionalidad
	/*
    //localidad
    $scope.localidad = function(){
        var reslocalidad = {
           "procedure_name":"fn_lst_localidad"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(reslocalidad)
        .success(function (response){
            $scope.localidad = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_localidad"]=error;
        });

    };
    //fin
    //provincia
    $scope.provincia = function(){
        var resprovincia = {
            "procedure_name":"fn_lst_provincia"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprovincia)
        .success(function (response){
            $scope.provincia = response;
            console.log("esta es la respuesta:" + response);  
        })
        .error(function(error){
            $scope.errors["error_reg_provincia"]=error;
        });

    };
    //fin
    //profesion
    $scope.profesion = function(){
        var resprofesion = {
            "procedure_name":"fn_lst_profesion"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resprofesion)
        .success(function (response){
            $scope.profesion = response;
        })
        .error(function(error){
            $scope.errors["error_reg_profesion"]=error;

        });
    };
    //fin

    */

    $scope.$on('api:ready',function(){
        $scope.getTitular();
		$scope.nacionalidad();
        //$scope.getEstados();
    });
    $scope.inicioTitular = function () {
        if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.getTitular();
			$scope.nacionalidad();
           // $scope.getEstados();
        }
    };    
    
});
