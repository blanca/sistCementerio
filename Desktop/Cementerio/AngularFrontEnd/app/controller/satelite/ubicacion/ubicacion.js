app.controller('ubicacionController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet) {
      $.blockUI();

$scope.getSeccion = function(){
        $.blockUI();
        var resSeccion = {
            "procedure_name":"fn_lst_l1locacion"
        };
        //servicio listar difuntos
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resSeccion );
        obj.success(function (response) {
            $scope.obtSeccion = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data es:" +  data);
            $scope.tablaDifunto = new ngTableParams({
                page: 1,          
                count: 5,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtSeccion.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtSeccion, params.filter()) :
                    $scope.obtSeccion;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtSeccion;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_difunto"] = error;  
            $.unblockUI();            
        });        
    };

      $scope.L1Secciones= function(){
      	var resL1secciones={
      		"procedure_name":"fn_lst_L1locacion"
      	};
      	//listar secciones
      	 var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resL1Secciones);
        obj.success(function(response){
            $scope.obtL1Secciones = response;
            var data = response;   //grabamos la respuesta para el paginado
            $scope.tablaUbicacion = new ngTableParams({
                page: 1,          
                count: 10,  
                filter: {},
                sorting: {}    
            }, {
                total: $scope.obtL1Secciones.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtL1Secciones, params.filter()) :
                    $scope.obtL1Secciones;              
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.obtL1Secciones;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
            $.unblockUI(); 
        })
        obj.error(function(error) {
            $scope.errors["error_Secciones"] = error;            
        });


      };

$scope.$on('api:ready',function(){
	$scope.L1Secciones();
});



$scope.limpiar = function(){
       /* $scope.getPersonasUsuarios("new");
        $scope.datosUsuario = '';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Usuarios";*/
    };
    $scope.modificarUsuarioCargar = function(usuario){
       /* $scope.getPersonasUsuarios("upd",usuario.psrId);
        $scope.datosUsuario=usuario;
        $scope.boton="upd";
        $scope.desabilitado=false;
        $scope.titulo="Modificar Usuarios";*/
    };
    $scope.eliminarUsuarioCargar = function(usuario){
       /* $scope.getPersonasUsuarios("upd",usuario.psrId);
        $scope.datosUsuario=usuario;
        $scope.desabilitado=true;
        $scope.boton="del";
        $scope.titulo="Eliminar Usuarios";*/
    };
$scope.adicionarPersona = function(datosPersona){
        /*$.blockUI();
        var persona = {};
        var fechaNacimiento=datosPersona.prsfecnmt.getFullYear() + "-" + datosPersona.prsfecnmt.getMonth() + "-" + datosPersona.prsfecnmt.getDate() + " " + datosPersona.prsfecnmt.getHours() + ":" + datosPersona.prsfecnmt.getMinutes() + ":" + datosPersona.prsfecnmt.getSeconds();
        persona['prs_nombres'] = datosPersona.prsNom; 
        persona['prs_id_estado_civil'] = datosPersona.prsStcvlId;
        persona['prs_ci'] = datosPersona.prsCi;
        persona['prs_paterno'] = datosPersona.prsPat;
        persona['prs_materno'] = datosPersona.prsMat;
        persona['prs_direccion'] = datosPersona.prsDireccion;
        persona['prs_telefono'] = datosPersona.prsTelefono;
        persona['prs_sexo'] = datosPersona.prsSexo;
        persona['prs_fec_nacimiento'] = fechaNacimiento;

        if(datosPersona.prsDireccion2)
        {   persona['prs_direccion2'] = datosPersona.prsDireccion2;   }
        else{   persona['prs_direccion2'] = '';    }

        if(datosPersona.prsTelefono2)
        {   persona['prs_telefono2'] = datosPersona.prsTelefono2;   }
        else{   persona['prs_telefono2'] = '';    }

        if(datosPersona.prsCelular)
        {   persona['prs_celular'] = datosPersona.prsCelular;   }
        else{   persona['prs_celular'] = '';    }

        if(datosPersona.prsEmpTel)
        {   persona['prs_empresa_telefonica'] = datosPersona.prsEmpTel;   }
        else{   persona['prs_empresa_telefonica'] = '';    }

        if(datosPersona.prsCorreo)
        {   persona['prs_correo'] = datosPersona.prsCorreo;   }
        else{   persona['prs_correo'] = '';    }

        persona['prs_id_archivo_cv'] = '1';
        persona['prs_registrado'] = fechactual;
        persona['prs_modificado'] = fechactual;
        persona['prs_usr_id'] = sessionService.get('IDUSUARIO');
        persona['prs_estado'] = 'A';
        
        var resPersona = {
            table_name:"_bp_personas",
            body:persona
        };
        //servicio insertar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resPersona);
        obj.success(function(data){
            $scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            $route.reload();
            $scope.getPersonas();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })*/
    };

    $scope.modificarDifunto = function(dif_id, datosDifunto){
       /*  $.blockUI();
        var difunto = {};
        var resDifunto = {
         "procedure_name":"fn_modificar_difunto(dif_codId)",
                    "body":{
                            "params":[ 
                                {
                                    "name": "dif_codifun",
                                    "value": datosDifunto.dif_id                                },

                                {
                                    "name": "ci_dif",
                                    "value": datosDifunto.nac_id
                                },                                
                                {
                                    "name": "nombre_dif",
                                    "value":datosDifunto.difNom
                                },
                                {
                                    "name": "paterno_dif",
                                    "value": datosDifunto.difPat
                                },
                                {
                                    "name": "materno_dif",
                                    "value": datosDifunto.difMat
                                },
                                {
                                    "name": "otroApellido_dif",
                                    "value": datosDifunto.difOtroApellido
                                },                                
                                {
                                    "name": "nacionalidad_dif",
                                    "value": datosDifunto.nac_id
                                },                                
                                {
                                    "name": "localidad_dif",
                                    "value": datosDifunto.loc_cod
                                },
                                {
                                    "name": "provincia_dif",
                                    "value":datosDifunto.prov_id
                                },
                                {
                                    "name": "ciudad_dif",
                                    "value": datosDifunto.cuidad
                                },
                                {
                                    "name": "fechaNacimiento_dif",
                                    "value": datosDifunto.fechaNacimiento
                                },
                                {
                                    "name": "sexo_dif",
                                    "value": datosDifunto.difSexo
                                },
                                {
                                    "name": "estadoCivil_dif",
                                    "value": datosDifunto.difEstCivil
                                },
                                {
                                    "name": "profesion_dif",
                                    "value": datosDifunto.difprofesion
                                },
                                {
                                    "name": "observacion_dif",
                                    "value": datosDifunto.difObservacion
                                },
                                {
                                    "name": "gestion_dif",
                                    "value": datosDifunto.difGestion
                                },
                                {
                                    "name": "edad_dif",
                                    "value": datosDifunto.difEdad
                                }
                                
                            ]
                         }
                     };
                
             var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function(data){
           // $scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro modificado', 'success');
            $route.reload();
            $scope.getDifuntos();
        })
        .error(function(data){
            sweet.show('', 'Registro no modificado', 'error');
            $.unblockUI();
        })*/
    };
$scope.eliminarPersona = function(prsId){
        /*$.blockUI();
        var persona = {};
        persona['prs_modificado'] = fechactual;
        persona['prs_estado'] = 'B';
        persona['prs_usr_id'] = sessionService.get('IDUSUARIO');

        var resPersona = {
            table_name:"_bp_personas",
            id:prsId,
            body:persona
        };
        //servicio eliminar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].updateRecord(resPersona);
        obj.success(function(data){
            $scope.getEstados();
            $.unblockUI(); 
            sweet.show('', 'Registro eliminado', 'success');
            $route.reload();
        })
        .error(function(data){
            sweet.show('', 'Registro no eliminado', 'error');
        })*/
    }; 

















$scope.inicioUbicacion= function(){
	if(DreamFactory.api[CONFIG.SERVICE]){
            $scope.L1Secciones();
           // $scope.getEstados();
        }

};

 $scope.limpiar = function(){
        /*$scope.getPersonasUsuarios("new");
        $scope.datosUsuario = '';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de Usuarios";*/
    };

    
});

