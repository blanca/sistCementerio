﻿
CREATE TABLE _bp_estados_civiles (
	estcivil_id serial PRIMARY KEY,
	estcivil text not null,
	estcivil_registrado timestamp NOT NULL DEFAULT now(),
	estcivil_modificado timestamp NOT NULL DEFAULT now(),
	estcivil_usr_id integer NOT NULL,
	estcivil_estado char(1) NOT NULL DEFAULT 'A'
);
INSERT INTO _bp_estados_civiles (estcivil, estcivil_usr_id, estcivil_estado) VALUES 
                                ('Soltero/a', 1, 'A'),
								('Casado/a', 1, 'A'),
								('Divorciado/a', 1, 'A'),
								('Viudo/a', 1, 'A'),
								('Prs. Jurídica', 1, 'A');


CREATE TABLE _bp_tipo_comunicacion (
	tpcmcc_id serial PRIMARY KEY,	
	tpcmcc_comunicacion text NOT NULL,	
	tpcmcc_registrado timestamp NOT NULL DEFAULT now(),
	tpcmcc_modificado timestamp NOT NULL DEFAULT now(),
	tpcmcc_usr_id integer NOT NULL,
	tpcmcc_estado char(1) NOT NULL DEFAULT 'A'
);
INSERT INTO _bp_tipo_comunicacion ( tpcmcc_comunicacion, tpcmcc_usr_id) VALUES 
								  ('Correo', 1),
								  ('Telefono', 1),
								  ('Movil', 1);


CREATE TABLE _bp_tipo_registros (
	tprgt_id serial PRIMARY KEY,	
	tprgt_regisrto text NOT NULL,	
	tprgt_registrado timestamp NOT NULL DEFAULT now(),
	tprgt_modificado timestamp NOT NULL DEFAULT now(),
	tprgt_usr_id integer NOT NULL,
	tprgt_estado char(1) NOT NULL DEFAULT 'A'
);

INSERT INTO _bp_tipo_registros( tprgt_regisrto, tprgt_usr_id) VALUES 
							  ('Plataforma', 1),
							  ('Autoregistro', 1);

CREATE TABLE _bp_macrodistrito (
	mcdstt_id serial PRIMARY KEY,	
	mcdstt_macrodistrito text NOT NULL,	
	mcdstt_subalcaldia text NOT NULL,	
	mcdstt_registrado timestamp NOT NULL DEFAULT now(),
	mcdstt_modificado timestamp NOT NULL DEFAULT now(),
	mcdstt_usr_id integer NOT NULL,
	mcdstt_estado char(1) NOT NULL DEFAULT 'A'
);

INSERT INTO _bp_macrodistrito( mcdstt_macrodistrito, mcdstt_subalcaldia,mcdstt_usr_id) VALUES 
						     ('COTAHUMA', 1,1),
							 ('MAX_PAREDES', 2,1),							  
							 ('PERIFERICA', 3,1),							  
							 ('SAN_ANTONIO', 4,1),							  
							 ('SUR', 5,1),							  
							 ('MALLASA', 6,1),							 
							 ('CENTRO', 7,1),							  
							 ('HAMPATURI', 8,1),							  
							 ('ZONGO', 9,1);							  


CREATE TABLE _bp_distrito (
	dstt_id serial PRIMARY KEY,	
	dstt_distrito text NOT NULL,	
	dstt_id_mcdstt integer NOT NULL,
	dstt_registrado timestamp NOT NULL DEFAULT now(),
	dstt_modificado timestamp NOT NULL DEFAULT now(),
	dstt_usr_id integer NOT NULL,
	dstt_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(dstt_id_mcdstt) REFERENCES _bp_macrodistrito(mcdstt_id)
);

INSERT INTO _bp_distrito( dstt_distrito, dstt_id_mcdstt,dstt_usr_id) VALUES 
						('DISTRITO 1',	7,1),
						('DISTRITO 2',	7,1),
						('DISTRITO 3',	1,1),
						('DISTRITO 4',	1,1),
						('DISTRITO 5',	1,1),
						('DISTRITO 6',	1,1),
						('DISTRITO 7',	2,1),
						('DISTRITO 8',	2,1),
						('DISTRITO 9',	2,1),
						('DISTRITO 10',	2,1),
						('DISTRITO 11',	3,1),
						('DISTRITO 12',	3,1),
						('DISTRITO 13',	3,1),
						('DISTRITO 14',	4,1),
						('DISTRITO 15',	4,1),
						('DISTRITO 16',	4,1),
						('DISTRITO 17',	4,1),
						('DISTRITO 18',	5,1),
						('DISTRITO 19',	5,1),
						('DISTRITO 20',	6,1),
						('DISTRITO 21',	5,1),
						('DISTRITO 22',	8,1),
						('DISTRITO 23',	9,1);
							  


CREATE TABLE _bp_zona (
	zn_id serial PRIMARY KEY,	
	zn_zona text NOT NULL,
    zn_id_dstt integer NOT NULL,	
	zn_registrado timestamp NOT NULL DEFAULT now(),
	zn_modificado timestamp NOT NULL DEFAULT now(),
	zn_usr_id integer NOT NULL,
	zn_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(zn_id_dstt) REFERENCES _bp_distrito(dstt_id)
);

INSERT INTO _bp_zona( zn_zona, zn_id_dstt,zn_usr_id) VALUES 
					('GARITA',1,	1),
					('BUENOS AIRES',2,	1);




CREATE TABLE _bp_datos_personales (
	dtspsl_id serial PRIMARY KEY,
	dtspsl_id_estado_civil integer NOT NULL,	
	dtspsl_id_tp_registro integer NOT NULL,	
	dtspsl_ci text NOT NULL,
	dtspsl_pin text NOT NULL,
	dtspsl_nombres text NOT NULL,
	dtspsl_paterno text NOT NULL,
	dtspsl_materno text NOT NULL,	
	dtspsl_sexo char(1) DEFAULT 'F',
	dtspsl_ocupacion text,
	dtspsl_fec_nacimiento date NOT NULL,
	dtspsl_activacionf char(2) NOT NULL DEFAULT 'NO',
	dtspsl_activaciond char(2) NOT NULL DEFAULT 'NO',
	dtspsl_fec_activacionf date,
	dtspsl_fec_activaciond date,
	dtspsl_observacion_activacion text,
	dtspsl_estado_activacion text NOT NULL DEFAULT 'DESBLOQUEADO',
	dtspsl_registrado timestamp NOT NULL DEFAULT now(),
	dtspsl_modificado timestamp NOT NULL DEFAULT now(),
	dtspsl_usr_id integer NOT NULL,
	dtspsl_estado char(1) NOT NULL DEFAULT 'A',

	dtspsl_tipo_persona text,
	dtspsl_poder_replegal text,
	dtspsl_nro_documento text,
	dtspsl_nro_notaria text,
	dtspsl_razon_social text,
	dtspsl_nit text,
	dtspsl_zona integer,
	dtspsl_distrito integer,
	dtspsl_macrodistrito integer,

	FOREIGN KEY(dtspsl_id_estado_civil) REFERENCES _bp_estados_civiles(estcivil_id),
	/*FOREIGN KEY(dtspsl_macrodistrito) REFERENCES gam_macrodistritos(mdst_id),
	FOREIGN KEY(dtspsl_distrito) REFERENCES gam_distritos(dst_id),
	FOREIGN KEY(dtspsl_zona) REFERENCES gam_zonas(zona_id),*/
	FOREIGN KEY(dtspsl_id_tp_registro) REFERENCES _bp_tipo_registros(tprgt_id)
);

INSERT INTO _bp_datos_personales( dtspsl_id_estado_civil, dtspsl_id_tp_registro,dtspsl_ci,dtspsl_pin,dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_sexo,dtspsl_ocupacion,dtspsl_fec_nacimiento,dtspsl_activacionf,dtspsl_activaciond,dtspsl_fec_activacionf,dtspsl_fec_activaciond,dtspsl_observacion_activacion,dtspsl_estado_activacion,dtspsl_usr_id) VALUES 
								( 1, 1,'6157896','13r45','RODOLFO ARTURO','VILLEGAS','','M','CARNICERO','07-07-1988','NO','NO',NULL,NOW(),'','DESACTIVADO',1);

CREATE TABLE _bp_datos_direccion (
	dtsdrc_id serial PRIMARY KEY,	
	dtsdrc_direccion text NOT NULL,	
	dtsdrc_id_zona integer NOT NULL,
	dtsdrc_id_dtspesonales integer NOT NULL,
	dtsdrc_registrado timestamp NOT NULL DEFAULT now(),
	dtsdrc_modificado timestamp NOT NULL DEFAULT now(),
	dtsdrc_usr_id integer NOT NULL,
	dtsdrc_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(dtsdrc_id_zona) REFERENCES _bp_zona(zn_id),
	FOREIGN KEY(dtsdrc_id_dtspesonales) REFERENCES _bp_datos_personales(dtspsl_id)
);

INSERT INTO _bp_datos_direccion( dtsdrc_direccion, dtsdrc_id_zona,dtsdrc_id_dtspesonales,dtsdrc_usr_id) VALUES 
							   ( 'calle 21', 1,1,1);    


CREATE TABLE _bp_datos_comunicacion (
	dtscmc_id serial PRIMARY KEY,
    dtscmc_id_dtspersonal integer NOT NULL,	
    dtscmc_id_tpcomunicacion integer NOT NULL,		
	dtscmc_validacion text NOT NULL  DEFAULT 'NO',
    dtscmc_referencia text,
	dtscmc_registrado timestamp NOT NULL DEFAULT now(),
	dtscmc_modificado timestamp NOT NULL DEFAULT now(),
	dtscmc_usr_id integer NOT NULL,
	dtscmc_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(dtscmc_id_dtspersonal) REFERENCES _bp_datos_personales(dtspsl_id),
	FOREIGN KEY(dtscmc_id_tpcomunicacion) REFERENCES _bp_tipo_comunicacion(tpcmcc_id)
);

INSERT INTO _bp_datos_comunicacion ( dtscmc_id_dtspersonal, dtscmc_id_tpcomunicacion,dtscmc_validacion,dtscmc_usr_id) VALUES 
								   (1,1,'NO',1);
								   

--********************datos de la administracion ***************************************************
CREATE TABLE _bp_personas (
	prs_id serial PRIMARY KEY,
	prs_id_estado_civil integer NOT NULL,
	prs_id_archivo_cv integer,
	prs_ci text NOT NULL,
	prs_nombres text NOT NULL,
	prs_paterno text NOT NULL,
	prs_materno text NOT NULL,
	prs_direccion text NOT NULL,
	prs_direccion2 text DEFAULT '',
	prs_telefono text DEFAULT '',
	prs_telefono2 text DEFAULT '',
	prs_celular text DEFAULT '',
	prs_empresa_telefonica text DEFAULT '',
	prs_correo text DEFAULT '',
	prs_sexo char(1) DEFAULT 'F',
	prs_fec_nacimiento date NOT NULL,
	prs_registrado timestamp NOT NULL DEFAULT now(),
	prs_modificado timestamp NOT NULL DEFAULT now(),
	prs_usr_id integer NOT NULL,
	prs_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(prs_id_estado_civil) REFERENCES _bp_estados_civiles(estcivil_id)
	

);

INSERT INTO _bp_personas (prs_id_estado_civil, prs_id_archivo_cv, prs_ci, prs_nombres, prs_paterno, prs_materno, prs_direccion, prs_direccion2, prs_telefono, prs_telefono2, prs_celular,prs_empresa_telefonica, prs_correo, prs_sexo, prs_fec_nacimiento, prs_usr_id, prs_estado) 
VALUES (1, 0, 1, 'Administrador', 'Admin', 'Admin', '', '', '', '', '', '', '', 'M', '2015-01-01', 1, 'A');
INSERT INTO _bp_personas (prs_id_estado_civil, prs_id_archivo_cv, prs_ci, prs_nombres, prs_paterno, prs_materno, prs_direccion, prs_direccion2, prs_telefono, prs_telefono2, prs_celular,prs_empresa_telefonica, prs_correo, prs_sexo, prs_fec_nacimiento, prs_usr_id, prs_estado) 
VALUES (1, 0, 1, 'Juan', 'Perez', 'Perez', '', '', '', '', '', '', '', 'M', '2015-01-01', 1, 'A');
INSERT INTO _bp_personas (prs_id_estado_civil, prs_id_archivo_cv, prs_ci, prs_nombres, prs_paterno, prs_materno, prs_direccion, prs_direccion2, prs_telefono, prs_telefono2, prs_celular,prs_empresa_telefonica, prs_correo, prs_sexo, prs_fec_nacimiento, prs_usr_id, prs_estado) 
VALUES (1, 0, 1, 'Plataformista', 'Plataformista', 'Plataformista', '', '', '', '', '', '', '', 'M', '2015-01-01', 1, 'A');




CREATE TABLE _bp_usuarios (
	usr_id serial PRIMARY KEY,
	usr_prs_id integer NOT NULL DEFAULT '1',
	usr_usuario text NOT NULL,
	usr_clave text NOT NULL,
	usr_controlar_ip char(1) NOT NULL DEFAULT 'S',
	usr_registrado timestamp NOT NULL DEFAULT now(),
	usr_modificado timestamp NOT NULL DEFAULT now(),
	usr_usr_id integer NOT NULL,
	usr_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(usr_prs_id) REFERENCES _bp_personas(prs_id)
);

INSERT INTO _bp_usuarios (usr_prs_id, usr_usuario, usr_clave, usr_controlar_ip, usr_usr_id, usr_estado) 
VALUES ( 1, 'admin', 'admin', 'N', 1, 'A');
INSERT INTO _bp_usuarios (usr_prs_id, usr_usuario, usr_clave, usr_controlar_ip, usr_usr_id, usr_estado)
VALUES ( 1, 'juan.perez', '123', 'N', 1, 'A');
INSERT INTO _bp_usuarios (usr_prs_id, usr_usuario, usr_clave, usr_controlar_ip, usr_usr_id, usr_estado)
VALUES ( 1, 'plataformista', '123', 'N', 1, 'A');



CREATE TABLE _bp_roles(
	rls_id serial PRIMARY KEY,
	rls_rol text NOT NULL,
	rls_registrado timestamp NOT NULL DEFAULT now(),
	rls_modificado timestamp NOT NULL DEFAULT now(),
	rls_usr_id integer NOT NULL,
	rls_estado char(1) NOT NULL DEFAULT 'A'
);

INSERT INTO _bp_roles (rls_rol, rls_usr_id, rls_estado) 
VALUES ('Super Usuario', 1, 'A');
INSERT INTO _bp_roles (rls_rol, rls_usr_id, rls_estado) 
VALUES ('Plataforma', 1, 'A');
INSERT INTO _bp_roles (rls_rol, rls_usr_id, rls_estado) 
VALUES ('Ciudadano', 1, 'A');




CREATE TABLE _bp_grupos(
	grp_id serial PRIMARY KEY,
	grp_grupo text NOT NULL,
	grp_imagen text DEFAULT '',
	grp_registrado timestamp NOT NULL DEFAULT now(),
	grp_modificado timestamp NOT NULL DEFAULT now(),
	grp_usr_id integer NOT NULL,
	grp_estado char(1) NOT NULL DEFAULT 'A'

);


INSERT INTO _bp_grupos (grp_grupo, grp_imagen, grp_usr_id, grp_estado) VALUES ('SEGURIDAD', '', 1, 'A');
INSERT INTO _bp_grupos (grp_grupo, grp_imagen, grp_usr_id, grp_estado) VALUES ('MONITOREO', '', 1, 'A');
INSERT INTO _bp_grupos (grp_grupo, grp_imagen, grp_usr_id, grp_estado) VALUES ('AREA PERSONAL', '', 1, 'A');



CREATE TABLE _bp_opciones(
	opc_id serial PRIMARY KEY,
	opc_grp_id integer NOT NULL,
	opc_opcion text NOT NULL,
	opc_contenido text DEFAULT '',
	opc_adicional text DEFAULT '',
	opc_orden integer,
	opc_imagen text DEFAULT '',
	opc_registrado timestamp NOT NULL DEFAULT now(),
	opc_modificado timestamp NOT NULL DEFAULT now(),
	opc_usr_id integer NOT NULL,
	opc_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(opc_grp_id) REFERENCES _bp_grupos(grp_id)
);

                                                                 
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Personas', '../administracion/personas/index.html', '', 20, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Usuarios', '../administracion/usuarios/index.html', '', 30, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Roles', '../administracion/roles/index.html', '', 30, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'UsuariosRoles', '../administracion/usuariosRoles/index.html', '', 30, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Grupos', '../administracion/grupos/index.html', '', 30, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Opciones', '../administracion/opciones/index.html', '', 30, '', 1, 'A');
 INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (1, 'Accesos', '../administracion/accesos/index.html', '', 30, '', 1, 'A');
                    
INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (2, 'Registro Ciudadano', '../registro_ciudadano/busqueda/index.html', '', 10, '', 1, 'A');
INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (3, 'Actualización de datos personales', '../registro_ciudadano/cambio/index.html', '', 10, '', 1, 'A');
INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (3, 'Cambio de Pin', '../registro_ciudadano/cambio/index.html', '', 10, '', 1, 'A');
INSERT INTO _bp_opciones (opc_grp_id, opc_opcion, opc_contenido, opc_adicional, opc_orden, opc_imagen, opc_usr_id, opc_estado) VALUES (3, 'Registro Medico', '../registro_ciudadano/regMedico/index.html', '', 10, '', 1, 'A');




CREATE TABLE _bp_accesos(
	acc_id serial PRIMARY KEY,
	acc_opc_id integer NOT NULL,
	acc_rls_id integer NOT NULL,
	acc_registrado timestamp NOT NULL DEFAULT now(),
	acc_modificado timestamp NOT NULL DEFAULT now(),
	acc_usr_id integer NOT NULL,
	acc_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(acc_opc_id) REFERENCES _bp_opciones(opc_id),
	FOREIGN KEY(acc_rls_id) REFERENCES _bp_roles(rls_id)
);

INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (1, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (2, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (3, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (4, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (5, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (6, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (7, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (8, 1, 1, 'A');
INSERT INTO _bp_accesos (acc_opc_id, acc_rls_id, acc_usr_id, acc_estado) VALUES (8, 3, 1, 'A');




CREATE TABLE _bp_usuarios_roles (
	usrls_id serial PRIMARY KEY,
	usrls_usr_id integer NOT NULL,
	usrls_rls_id integer NOT NULL,
	usrls_registrado timestamp NOT NULL DEFAULT now(),
	usrls_modificado timestamp NOT NULL DEFAULT now(),
	usrls_usuarios_usr_id integer NOT NULL,
	usrls_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(usrls_usr_id) REFERENCES _bp_usuarios(usr_id),
	FOREIGN KEY(usrls_rls_id) REFERENCES _bp_roles(rls_id)
);

INSERT INTO _bp_usuarios_roles (usrls_usr_id, usrls_rls_id, usrls_usuarios_usr_id, usrls_estado) VALUES (1, 1, 1, 'A');
INSERT INTO _bp_usuarios_roles (usrls_usr_id, usrls_rls_id, usrls_usuarios_usr_id, usrls_estado) VALUES (2, 2, 1, 'A');
INSERT INTO _bp_usuarios_roles (usrls_usr_id, usrls_rls_id, usrls_usuarios_usr_id, usrls_estado) VALUES (3, 3, 1, 'A');




CREATE TABLE _bp_logs_usuarios (
	lgsusr_id_log serial PRIMARY KEY,
	lgsusr_usr_id integer NOT NULL,
	lgsusr_ip text NOT NULL,
	lgsusr_tipo char(1) NOT NULL,
	lgsusr_registrado  timestamp NOT NULL DEFAULT now(),
	lgsusr_modificado timestamp NOT NULL DEFAULT now(),
	lgsusr_usuarios_usr_id integer NOT NULL,
	lgsusr_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(lgsusr_usr_id) REFERENCES _bp_usuarios(usr_id)
);

CREATE TABLE _bp_logs_eventos
(
  lgs_id_log serial PRIMARY KEY,
  lgs_usr_id integer NOT NULL,
  lgs_ip text,
  lgs_evento text NOT NULL,
  lgs_registrado timestamp without time zone NOT NULL DEFAULT now(),
  lgs_modificado timestamp without time zone NOT NULL DEFAULT now(),
  lgs_estado character(1) NOT NULL DEFAULT 'A'::bpchar,
  FOREIGN KEY(lgs_usr_id) REFERENCES _bp_usuarios (usr_id)  
);

CREATE TABLE _bp_datos_medicos (
  dtsmdc_id serial PRIMARY KEY,
  dtsmdc_dtspsl_id integer NOT NULL,
  dtsmdc_grp_sanguineo text,
  dtsmdc_cbt_medica text,
  dtsmdc_fecha_alta date,
  dtsmdc_ant_personal_1 text,
  dtsmdc_ant_personal_2 text,
  dtsmdc_ant_personal_3 text,
  dtsmdc_ant_familiar_1 text,
  dtsmdc_ant_familiar_2 text,
  dtsmdc_ant_familiar_3 text,
  dtsmdc_observaciones text,
  dtsmdc_registrado  timestamp NOT NULL DEFAULT now(),
  dtsmdc_modificado timestamp NOT NULL DEFAULT now(),
  dtsmdc_usr_id integer NOT NULL,
  dtsmdc_estado char(1) NOT NULL DEFAULT 'A' ,
  FOREIGN KEY(dtsmdc_dtspsl_id) REFERENCES _bp_datos_personales (dtspsl_id)   
);

								   
								  
---***********************

CREATE TABLE _bp_categorias (
	ctgr_id serial PRIMARY KEY,	
	ctgr_descripcion text NOT NULL,
	ctgr_padre_id INTEGER REFERENCES _bp_categorias(ctgr_id),
	ctgr_registrado timestamp NOT NULL DEFAULT now(),
	ctgr_modificado timestamp NOT NULL DEFAULT now(),
	ctgr_usr_id integer NOT NULL,
	ctgr_estado char(1) NOT NULL DEFAULT 'A'
);
INSERT INTO _bp_categorias (ctgr_descripcion, ctgr_padre_id, ctgr_usr_id) VALUES ('libro', null, 1);
INSERT INTO _bp_categorias (ctgr_descripcion, ctgr_padre_id, ctgr_usr_id) VALUES ('revista', null, 1);
INSERT INTO _bp_categorias (ctgr_descripcion, ctgr_padre_id, ctgr_usr_id) VALUES ('c#', 1, 1);
INSERT INTO _bp_categorias (ctgr_descripcion, ctgr_padre_id, ctgr_usr_id) VALUES ('people', 2, 1);

CREATE TABLE _bp_doc_recibidos (
	docrbds_id serial PRIMARY KEY,
	docrbds_id_dtspesonales integer,	
	docrbds_descripcion text NOT NULL,
	docrbds_resumen text NOT NULL,
	docrbds_url text NOT NULL,
	docrbds_id_categoria integer NOT NULL,	
	docrbds_registrado timestamp NOT NULL DEFAULT now(),
	docrbds_modificado timestamp NOT NULL DEFAULT now(),
	docrbds_usr_id integer NOT NULL,
	docrbds_estado char(1) NOT NULL DEFAULT 'A',
	FOREIGN KEY(docrbds_id_categoria) REFERENCES _bp_categorias(ctgr_id),
	FOREIGN KEY(docrbds_id_dtspesonales) REFERENCES _bp_datos_personales(dtspsl_id)	
);
INSERT INTO _bp_doc_recibidos (docrbds_id_dtspesonales, docrbds_descripcion,docrbds_resumen,docrbds_url,docrbds_id_categoria, docrbds_usr_id) VALUES (1,'libros de programacion', 'relacionados a estudiantes de informastica', 'D:\data\libros\otro.pdf',1,1);

/*************************************************/
CREATE TABLE _ciudadano_evento
(
  cev_id serial  PRIMARY KEY,
  cev_dtspsl_id integer,
  cev_id_usuario integer,
  cev_evento text NOT NULL,
  cev_fech_inicio date NOT NULL DEFAULT now(),
  cev_fech_fin date NOT NULL DEFAULT now(),
  cev_hora_inicio time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
  cev_hora_fin time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
  cev_registrado timestamp without time zone NOT NULL DEFAULT now(),
  cev_modificado timestamp without time zone NOT NULL DEFAULT now(),
  cev_estado char(1) NOT NULL DEFAULT 'A',
  cev_tipo text,
  FOREIGN KEY(cev_dtspsl_id) REFERENCES _bp_datos_personales(dtspsl_id)	
);

CREATE TABLE _ciudadano_noticias
(
  cnt_id serial  PRIMARY KEY,
  cnt_titulo text NOT NULL,
  cnt_resumen text NOT NULL,
  cnt_url text NOT NULL,
  cnt_imagen text NOT NULL,
  cnt_visible char(2) NOT NULL DEFAULT 'SI',
  cnt_id_usuario integer,
  cnt_registrado timestamp DEFAULT now(),
  cnt_modificado timestamp DEFAULT now(),
  cnt_estado char(1) NOT NULL DEFAULT 'A'
);
-- DROP TABLE _ciudadano_archivos;

CREATE TABLE _ciudadano_archivos
(
  crch_id_archivo serial PRIMARY KEY,
  crch_ciudadano_id integer NOT NULL,
  crch_proceso text,
  crch_actividad text,
  crch_nombre text,
  crch_tamano text,
  crch_direccion text,
  crch_id_prc_act_vista text,
  crch_direccion2 text,
  crch_id_historico text,
  crch_id_uo text,
  crch_registrado timestamp  DEFAULT now(),
  crch_modificado timestamp  DEFAULT now(),
  crch_id_usuario integer NOT NULL,
  crch_estado char(1) NOT NULL DEFAULT 'A',
  FOREIGN KEY(crch_ciudadano_id) REFERENCES _bp_datos_personales(dtspsl_id)
);

CREATE TABLE _servicio_div
(
  serdv_id serial PRIMARY KEY,
  serdv_descripcion text,
  serdv_servicio text NOT NULL,
  serdv_url text,
  serdv_tdk text,
  serdv_registrado timestamp  DEFAULT now(),
  serdv_modificado timestamp  DEFAULT now(),
  serdv_estado char(1) NOT NULL DEFAULT 'A'
);

CREATE TABLE _formulario_datos
(
  frm_id serial PRIMARY KEY,
  frm_dvser_id integer,
  frm_dato text NOT NULL,
  frm_id_ciudadano integer,
  frm_id_usuario integer,
  frm_registrado timestamp  DEFAULT now(),
  frm_modificado timestamp  DEFAULT now(),
  frm_estado char(1) NOT NULL DEFAULT 'A',
  frm_id_tramite bigint,
  FOREIGN KEY(frm_dvser_id) REFERENCES _servicio_div(serdv_id)	
);
CREATE TABLE _formulario_tramites
(
  frm_tra_id serial PRIMARY KEY,
  frm_tra_dvser_id integer,
  frm_tra_id_ciudadano integer,
  frm_tra_fecha text NOT NULL,
  frm_tra_enviado text NOT NULL,
  frm_tra_id_usuario integer,
  frm_tra_registrado timestamp  DEFAULT now(),
  frm_tra_modificado timestamp DEFAULT now(),
  frm_tra_estado char(1) NOT NULL DEFAULT 'A',
  FOREIGN KEY(frm_tra_dvser_id) REFERENCES _servicio_div(serdv_id)	
);

CREATE TABLE _publicidad_puntos
(
  pblpnt_id serial  PRIMARY KEY,
  pblpnt_direccion text NOT NULL,
  pblpnt_latitud double precision NOT NULL,
  pblpnt_longitud double precision NOT NULL,
  pblpnt_registrado timestamp  DEFAULT now(),
  pblpnt_modificado timestamp  DEFAULT now(),
  pblpnt_usr_id integer NOT NULL,
  pblpnt_estado char(1) NOT NULL DEFAULT 'A',
  pblpnt_tipo text
);



CREATE TABLE _publicidad_calendario
(
  pblcal_id serial  PRIMARY KEY,
  pblcal_id_usuario integer,
  pblcal_pblpnt_id integer,
  pblcal_tipo text,
  pblcal_fech_inicio date NOT NULL DEFAULT now(),
  pblcal_fech_fin date NOT NULL DEFAULT now(),
  pblcal_hora_inicio time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
  pblcal_hora_fin time without time zone NOT NULL DEFAULT '00:00:00'::time without time zone,
  pblcal_registrado timestamp  DEFAULT now(),
  pblcal_modificado timestamp  DEFAULT now(),
  pblcal_estado char(1) NOT NULL DEFAULT 'A',
    FOREIGN KEY(pblcal_pblpnt_id) REFERENCES _publicidad_puntos(pblpnt_id)

);

CREATE TABLE _publicidad_categoria
(
  pblcat_id serial PRIMARY KEY,
  pblcat_pblpnt_id serial NOT NULL,
  pblcat_descripcion text NOT NULL,
  pblcat_registrado timestamp DEFAULT now(),
  pblcat_modificado timestamp DEFAULT now(),
  pblcat_usr_id integer NOT NULL,
  pblcat_estado char(1) NOT NULL DEFAULT 'A',
   FOREIGN KEY(pblcat_pblpnt_id) REFERENCES _publicidad_puntos(pblpnt_id)
);
CREATE TABLE _publicidad_posicion
(
  pblpsc_id serial PRIMARY KEY,
  pblpsc_pblcat_id serial NOT NULL,
  pblpsc_posicion text NOT NULL,
  pblpsc_alto double precision NOT NULL,
  pblpsc_ancho double precision NOT NULL,
  pblpsc_registrado timestamp DEFAULT now(),
  pblpsc_modificado timestamp DEFAULT now(),
  pblpsc_usr_id integer NOT NULL,
  pblpsc_estado char(1) NOT NULL DEFAULT 'A',
  FOREIGN KEY(pblpsc_pblcat_id) REFERENCES _publicidad_categoria(pblcat_id)
);

CREATE TABLE _servicio_datos_persona
(
  serdtper_id serial PRIMARY KEY,
  serdtper_id_servicio integer NOT NULL,
  serdtper_id_dt_persona integer NOT NULL,
  serdtper_registrado timestamp DEFAULT now(),
  serdtper_modificado timestamp DEFAULT now(),
  serdtper_estado char(1) NOT NULL DEFAULT 'A',
  FOREIGN KEY(serdtper_id_dt_persona) REFERENCES _bp_datos_personales(dtspsl_id),
  FOREIGN KEY(serdtper_id) REFERENCES _servicio_div(serdv_id) 
);


CREATE TABLE count
(
  dtspsl_activacionf character(2)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE count OWNER TO postgres;