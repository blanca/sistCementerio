 CREATE OR REPLACE FUNCTION autenticacion(IN usrid text, IN usuario text)
  RETURNS TABLE(idusr integer, usr text, nombre text, paterno text, materno text, idrl integer, rol text, vci text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT usr_id,usr_usuario,prs_nombres,prs_paterno,prs_materno,rls_id,rls_rol,prs_ci
     FROM _bp_usuarios 
     INNER JOIN _bp_personas ON  prs_id=usr_prs_id  and prs_estado='A'
     INNER JOIN _bp_usuarios_roles ON usrls_usr_id=usr_id  and usrls_estado='A'
     INNER  JOIN _bp_roles ON usrls_rls_id=rls_id and rls_estado='A'
     where usr_usuario=usrId  AND usr_clave=usuario AND usr_estado='A' ORDER BY usrls_registrado DESC ;       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  CREATE OR REPLACE FUNCTION menu(IN idusr integer)
  RETURNS TABLE(idgrp integer, grp text, idopcion integer, opcion text, contenido text) AS
$BODY$
DECLARE
        
          i integer;
          j integer;
         con integer;
         conn text;
         var integer;
         var1 text;
         var2 text;
         idgr integer;
          
BEGIN 
    i := 1;
    CREATE LOCAL TEMPORARY TABLE "grupo" (
     id serial PRIMARY KEY,
    id_grupo integer NOT NULL,
    grupo text NOT NULL ) WITH OIDS;
    INSERT INTO grupo
     (id_grupo, grupo)  
    (SELECT DISTINCT (gr.grp_id) as id, gr.grp_grupo 
    FROM _bp_accesos   
    INNER JOIN _bp_opciones  ON acc_opc_id = opc_id
    INNER JOIN _bp_grupos gr ON grp_id = opc_grp_id
    INNER JOIN _bp_roles  ON rls_id = acc_rls_id
    INNER JOIN _bp_usuarios_roles ur ON usrls_rls_id = acc_rls_id
    WHERE usrls_usr_id = idUsr AND acc_estado = 'A' AND usrls_estado='A');
    CREATE LOCAL TEMPORARY TABLE "menu" 
    (id_grupo1 integer NOT NULL,grupo1 text NOT NULL,id_opcion1 integer NOT NULL,opcion1 text NOT NULL,contenido1 text NOT NULL ) WITH OIDS;
    WHILE (i<=(SELECT COUNT(*)FROM grupo)) LOOP
         SELECT g.id_grupo into idgr FROM grupo AS g WHERE g.id=i;
  CREATE TEMPORARY TABLE lstOpciones
        (id serial PRIMARY KEY,id_opcion integer NOT NULL,opcion text NOT NULL,contenido text NOT NULL);
         INSERT INTO lstOpciones  (id_opcion, opcion,contenido)
         (SELECT distinct  op.opc_id, op.opc_opcion, op.opc_contenido
    FROM _bp_accesos ac
    INNER JOIN _bp_opciones op ON ac.acc_opc_id = op.opc_id 
    INNER JOIN _bp_grupos gr ON gr.grp_id = op.opc_grp_id
    WHERE ac.acc_rls_id in (SELECT usrls_rls_id 
    FROM _bp_usuarios_roles
    WHERE usrls_usr_id = idUsr AND usrls_estado = 'A' ) AND ac.acc_estado = 'A' 
    and op.opc_estado = 'A' and op.opc_grp_id= idgr order by op.opc_id);
    j=1;
    WHILE (j<=(SELECT COUNT(*)FROM lstOpciones))LOOP
    SELECT g.id_grupo into con FROM grupo as g  WHERE g.id=i ; 
          SELECT gr.grupo into conn FROM grupo as gr WHERE gr.id=i ; 
          SELECT ls.id_opcion INTO var FROM lstOpciones as ls WHERE ls.id=j; 
          SELECT lst.opcion INTO var1 FROM lstOpciones as lst WHERE lst.id=j; 
          SELECT lss.contenido INTO var2 FROM lstOpciones as lss WHERE lss.id=j;     
    INSERT INTO menu VALUES(con,conn,var,var1,var2);
    j = j+1;
    END LOOP;
    drop table lstOpciones;
    i = i+1;
    END LOOP;
    RETURN QUERY SELECT m.id_grupo1,m.grupo1,m.id_opcion1,m.opcion1,m.contenido1
    FROM "menu" as m order by m.id_grupo1 ; 
    drop table menu;
    drop table grupo;
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  --  FUNCTION accesoslst();

CREATE OR REPLACE FUNCTION accesoslst()
  RETURNS TABLE("accId" integer, "accOpcId" integer, "opcOpcion" text, "accRlsId" integer, "rlsRol" text, "accRegistrado" timestamp without time zone, "accModificado" timestamp without time zone) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT acc_id as "acc_id",acc_opc_id as "acc_opc_id",opc_opcion as "opc_opcion",acc_rls_id as "acc_rls_id",rls_rol as "rls_rol ",acc_registrado as "acc_registrado" ,acc_modificado as "acc_modificado"
            FROM _bp_accesos ac
            INNER JOIN _bp_opciones  ON opc_id =  acc_opc_id
            INNER JOIN _bp_roles  ON rls_id = acc_rls_id
            WHERE  acc_estado = 'A' ;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  --  FUNCTION accesosroles();
   CREATE OR REPLACE FUNCTION accesosroles(IN idrl integer)
  RETURNS TABLE(accid integer, opcid integer, opcion text, "rolID" integer, rol text, registrado timestamp without time zone, modificado timestamp without time zone) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT acc_id as "acc_id",acc_opc_id as "acc_opc_id",opc_opcion as "opc_opcion",acc_rls_id as "acc_rls_id",rls_rol as "rls_rol ",acc_registrado as "acc_registrado" ,acc_modificado as "acc_modificado"
            FROM _bp_accesos ac
            INNER JOIN _bp_opciones  ON opc_id =  acc_opc_id
            INNER JOIN _bp_roles  ON rls_id = acc_rls_id
            WHERE  acc_estado = 'A'  AND acc_rls_id= idrl;
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



CREATE OR REPLACE FUNCTION gruposlst(IN usrid integer)
  RETURNS TABLE(grpid integer, grp text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT DISTINCT (gr.grp_id) as id, gr.grp_grupo 
    FROM _bp_accesos   
    INNER JOIN _bp_opciones  ON acc_opc_id = opc_id
    INNER JOIN _bp_grupos gr ON grp_id = opc_grp_id
    INNER JOIN _bp_roles  ON rls_id = acc_rls_id
    INNER JOIN _bp_usuarios_roles ur ON usrls_rls_id = acc_rls_id
        WHERE usrls_usr_id = usrId AND acc_estado = 'A' AND usrls_estado='A';       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;





  
  CREATE OR REPLACE FUNCTION gruposlst()
  RETURNS TABLE("grpId" integer, "grpGrupo" text, "grpImagen" text, "grpRegistrado" timestamp without time zone, "grpModificado" timestamp without time zone, "grpEstado" text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT grp_id as "grp_id",grp_grupo as "grp_grupo",grp_imagen as "grp_imagen",grp_registrado as "grp_registrado",grp_modificado as "grp_modificado",
    CASE WHEN grp_estado='I' THEN 'INACTIVO' WHEN grp_estado='A' THEN 'ACTIVO' END AS grp_estado
	FROM _bp_grupos
	WHERE grp_estado <> 'B' 
	ORDER BY grp_id ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --*************************************************opcioneslibres
CREATE OR REPLACE FUNCTION opcioneslibres(IN usrid integer)
  RETURNS TABLE(grupo text, opcion text, contenido text, "opcId" integer, "usrId" integer) AS
$BODY$
BEGIN    
    RETURN QUERY  select grp_grupo, opc_opcion, opc_contenido, opc_id,opc_usr_id 
    from _bp_opciones
    inner join _bp_grupos ON opc_grp_id = grp_id
    where opc_estado = 'A' and opc_id not in (select acc_opc_id from _bp_accesos where acc_estado = 'A' and acc_rls_id = usrId)
    order by opc_id;
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  
  

CREATE OR REPLACE FUNCTION opcioneslst()
  RETURNS TABLE("opcId" integer, "opcGrpId" integer, "opcGrp" text, "opcOpcion" text, "opcContenido" text, "opcAdicional" text, "opcOrden" integer, "opcImagen" text, "opcRegistrado" timestamp without time zone, "opcModificado" timestamp without time zone, "opcEstado" text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT opc_id as "opc_id",grp_id as "grp_id",grp_grupo as "grp_grupo",opc_opcion as "opc_opcion",opc_contenido as "opc_contenido",opc_adicional as "opc_adicional",opc_orden as "opc_orden",opc_imagen as "opc_imagen",opc_registrado as "opc_registrado",
opc_modificado as "opc_modificado", CASE WHEN opc_estado='A' THEN 'ACTIVO' END AS opc_estado
      FROM _bp_opciones 
      INNER JOIN _bp_grupos  ON grp_id = opc_grp_id
      WHERE  opc_estado <> 'B'  
      ORDER BY  opc_id ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  --*********************************persona_list_no_usuario
CREATE OR REPLACE FUNCTION persona_list_no_usuario()
  RETURNS TABLE("idPrs" integer, "Prs" text) AS
$BODY$
BEGIN    
    RETURN QUERY select concat(prs_nombres,' ',prs_paterno,' ',prs_materno) as persona,prs_id
           from _bp_personas
                 where prs_id not in ( select usr_prs_id from _bp_usuarios where usr_estado='A')  AND PRS_ESTADO='A'
                 ORDER BY persona ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--*********************************persona_list_no_usuario_new
CREATE OR REPLACE FUNCTION persona_list_no_usuario_new()
  RETURNS TABLE("Prs" text, "idPrs" integer) AS
$BODY$
BEGIN    
    RETURN QUERY select concat(prs_nombres,' ',prs_paterno,' ',prs_materno) as persona,prs_id
           from _bp_personas
                 where prs_id not in ( select usr_prs_id from _bp_usuarios where usr_estado<>'B') AND PRS_ESTADO<>'B'
                 ORDER BY persona ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --******************************persona_list_no_usuario_upd
CREATE OR REPLACE FUNCTION persona_list_no_usuario_upd(IN idpsr integer)
  RETURNS TABLE("Prs" text, "idPrs" integer) AS
$BODY$
BEGIN    
    RETURN QUERY select concat(prs_nombres,' ',prs_paterno,' ',prs_materno) as persona,prs_id
           from _bp_personas
                 where prs_id=idpsr AND PRS_ESTADO<>'B'
                 ORDER BY persona ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --*******************************personaslst
CREATE OR REPLACE FUNCTION personaslst()
  RETURNS TABLE("prsId" integer, "prsCi" text, "prsNombre" text, "prsDireccion" text, "prsTelefono" text, "prsCelular" text, "prsCorreo" text, "prsSexo" character, prsfecnmt date, "prsStcvl" text, "prsStcvlId" integer, "prsNom" text, "prsPat" text, "prsMat" text, "prsDireccion2" text, "prsTelefono2" text, "prsEmpTel" text, "prsEstado" text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT prs_id, prs_ci, concat(prs_nombres,' ',prs_paterno,' ',prs_materno) as persona,
      prs_direccion, prs_telefono, prs_celular, prs_correo, prs_sexo, prs_fec_nacimiento, estcivil,
      prs_id_estado_civil,prs_nombres, prs_paterno, prs_materno, prs_direccion2, prs_telefono2,prs_empresa_telefonica,
      CASE 
        WHEN prs_estado='A' THEN 'ACTIVO' 
        WHEN prs_estado='I' THEN 'INACTIVO' END AS prs_estado 
    FROM _bp_personas 
    INNER JOIN _bp_estados_civiles ON prs_id_estado_civil=estcivil_id 
    WHERE prs_estado<>'B' ;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  --****************************roles_asignados_usuario
 CREATE OR REPLACE FUNCTION roles_asignados_usuario(IN idusr integer)
  RETURNS TABLE(usrlsid integer, usr text, rol text) AS
$BODY$
BEGIN    
    RETURN QUERY select usrls_id, usr_usuario, rls_rol
    from _bp_usuarios_roles inner join _bp_roles on usrls_rls_id=rls_id and rls_estado<>'B'
    inner join _bp_usuarios on usr_id=usrls_usr_id and usr_estado<>'B'
    where usrls_usr_id=idusr and usrls_estado<>'B';      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --********************************roles_no_asignados
CREATE OR REPLACE FUNCTION roles_no_asignados(IN idusr integer)
  RETURNS TABLE(rol text, rlid integer) AS
$BODY$
BEGIN    
    RETURN QUERY select rls_rol, rls_id
         from _bp_roles
         where rls_estado='A' and rls_id not in (select usrls_rls_id  
         from _bp_usuarios_roles
         where usrls_usr_id=idusr and  usrls_estado='A');      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --********************************roleslst
 CREATE OR REPLACE FUNCTION roleslst()
  RETURNS TABLE("rlsId" integer, "rlsRol" text, "rlsRegistrado" timestamp without time zone, "rlsModificado" timestamp without time zone, "rlsEstado" text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT rls_id, rls_rol,rls_registrado, rls_modificado,
      CASE 
        WHEN rls_estado='A' THEN 'ACTIVO' 
        WHEN rls_estado='I' THEN 'INACTIVO' END AS rls_estado 
      FROM _bp_roles
      WHERE rls_estado<>'B' ;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --******************************roleslst_id
CREATE OR REPLACE FUNCTION roleslst_id(IN rol_id integer)
  RETURNS TABLE(rls_id integer, rls_rol text) AS
$BODY$
BEGIN    
    RETURN QUERY  SELECT rls_id, rls_rol
      FROM _bp_roles
      WHERE rls_estado='A' AND rls_id =  rol_id;     
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --****************************stdcivilst
CREATE OR REPLACE FUNCTION stdcivilst()
  RETURNS TABLE("estcivilId" integer, "estCivil" text, "estcivilRegistrado" timestamp without time zone, "estcivilModificado" timestamp without time zone) AS
$BODY$
BEGIN    
    RETURN QUERY  SELECT  estcivil_id as "estcivil_id",estcivil as "estcivil",estcivil_registrado as "estcivil_registrado",estcivil_modificado as "estcivil_modificado"
  FROM _bp_estados_civiles
  INNER JOIN _bp_usuarios ON usr_id = estcivil_usr_id
  WHERE estcivil_estado = 'A' ORDER BY estcivil_id ASC;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  
  --********************************usuarioslst
CREATE OR REPLACE FUNCTION usuarioslst()
  RETURNS TABLE("usrId" integer, "usrUsuario" text, "usrNombre" text, "usrClave" text, "psrId" integer, "usrEstado" text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT usr_id, usr_usuario, concat(prs_nombres,' ',prs_paterno,' ',prs_materno) as persona, usr_clave,usr_prs_id,
    CASE 
        WHEN usr_estado='A' THEN 'ACTIVO' 
        WHEN usr_estado='I' THEN 'INACTIVO' END AS usr_estado 
    FROM _bp_usuarios INNER JOIN _bp_personas ON prs_id=usr_prs_id 
    WHERE usr_estado<>'B' AND prs_estado<>'B' ;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --*********************************usuariosroleslst
  
CREATE OR REPLACE FUNCTION usuariosroleslst()
  RETURNS TABLE("usrlsId" integer, "usrUsuario" text, "rlsRol" text, "usrlsRegistrado" timestamp without time zone, "usrlsModificado" timestamp without time zone) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT usrls_id, usr_usuario, rls_rol, 
    usrls_registrado, usrls_modificado
    FROM _bp_usuarios_roles INNER JOIN _bp_usuarios ON usrls_usr_id=usr_id 
    INNER JOIN _bp_roles ON usrls_rls_id=rls_id ;      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
 --*********************************************registro ciudadano**********************************
--********************************************** Cambia pin****************************************
CREATE OR REPLACE FUNCTION spcambiopin(sci text, spinanterior text, spinnuevo text)
  RETURNS SETOF text AS
$BODY$
DECLARE
        idDtsP integer;
        confirmacio TEXT;
BEGIN 
   IF (  SELECT 1
    from _bp_datos_personales
          where dtspsl_ci=sci and    dtspsl_pin=spinanterior and   dtspsl_estado='A') THEN 
    select  dtspsl_id INTO idDtsP from _bp_datos_personales
          where dtspsl_ci=sci and    dtspsl_pin=spinanterior and   dtspsl_estado='A';

    IF idDtsP <> 0 THEN 
      UPDATE _bp_datos_personales SET dtspsl_pin=spinnuevo WHERE dtspsl_id = idDtsP and dtspsl_estado='A' ;
       confirmacio=('Pin modificado' );     
      RETURN QUERY SELECT   ( confirmacio);
    
     END IF;
   ELSE
   confirmacio=('Pin incorrecto o ciudadano no registrado' );      
      RETURN QUERY SELECT   confirmacio;
   END IF;
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- FUNCTION splistaciudadano();
CREATE OR REPLACE FUNCTION splistaciudadano()
  RETURNS TABLE(vid integer, vnombres text) AS
$BODY$
BEGIN 
     
      RETURN QUERY SELECT dtspsl_id,dtspsl_nombres FROM _bp_datos_personales;  
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--  FUNCTION spdesbloquearregistrociudadano(integer, text, integer);
CREATE OR REPLACE FUNCTION spdesbloquearregistrociudadano(dtspsidregistrosl_id integer, sobservacion text, sidusuario integer)
  RETURNS SETOF text AS
$BODY$
DECLARE
        
          count integer;               
          ciudadano TEXT;          
          confirmacio text;       
          
BEGIN    

    SELECT COUNT(*) INTO count
    FROM  _bp_datos_personales
    WHERE dtspsl_id =dtspsIdRegistrosl_id AND dtspsl_estado_activacion LIKE 'B';

    IF count >=1 THEN 
      confirmacio=('Desbloqueado hoy' );
  UPDATE _bp_datos_personales
        SET dtspsl_estado_activacion='N',        
         dtspsl_modificado=NOW(),
         dtspsl_usr_id=sIdusuario,
         dtspsl_observacion_activacion=sObservacion
        WHERE dtspsl_id =dtspsIdRegistrosl_id AND dtspsl_estado='A';      
      RETURN QUERY SELECT   confirmacio;
    ELSE 
     SELECT (dtspsl_nombres ||' ' || dtspsl_paterno ||' ' || dtspsl_materno) INTO ciudadano  
      FROM  _bp_datos_personales
      WHERE dtspsl_id =dtspsIdRegistrosl_id and  dtspsl_estado_activacion LIKE 'N' and dtspsl_estado='A';
       confirmacio=('Desbloqueado ciudadano ==> ' );      
      RETURN QUERY SELECT   ( confirmacio || ciudadano);      
     END IF;  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
-- ************************Busca registo ciudadano******************
CREATE OR REPLACE FUNCTION spbuscarregistrociudadano(IN sci text, IN snombre text, IN sappaterno text, IN sapmaterno text, IN sfechanacimiento date)
  RETURNS TABLE(vid integer, vci text, vnombre text, vappaterno text, vapmaterno text, vfechanacimiento date, vactivacionf character, vactivaciond character, vfec_activacionf date, vfec_activaciond date, vobservacion_activacion text, vestado_activacion text, vtipo_persona text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT   dtspsl_id, dtspsl_ci,dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_fec_nacimiento,dtspsl_activacionf,dtspsl_activaciond,dtspsl_fec_activacionf,dtspsl_fec_activaciond,dtspsl_observacion_activacion,dtspsl_estado_activacion,dtspsl_tipo_persona
     FROM _bp_datos_personales
                 WHERE   ((sCi IS NULL)OR(dtspsl_ci LIKE '%'||sCi||'%')) AND  dtspsl_estado='A' 
                 AND ((sNombre IS NULL)OR(dtspsl_nombres LIKE '%'||sNombre||'%'))
                 AND ((sApPaterno IS NULL)OR(dtspsl_paterno LIKE '%'||sApPaterno||'%'))
                 AND ((sApMaterno IS NULL)OR(dtspsl_materno LIKE '%'||sApMaterno||'%'))
                 AND ((sFechaNacimiento IS NULL)OR(dtspsl_fec_nacimiento = sFechaNacimiento))
                  AND dtspsl_tipo_persona = 'N'
                  ;       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
 
--**********************ACTIVACION DIGITAL*************************
CREATE OR REPLACE FUNCTION spactivaciondigital(sci text, spin text, sestadoactivaciondigital character, sidregistro integer)
  RETURNS SETOF text AS
$BODY$
DECLARE
        
          cantidad integer;
          FechActvacionDD date;         
          confirmacio text;
          respuesta char(2);
         
          
BEGIN 
   IF (  SELECT 1
    FROM  _bp_datos_personales
    WHERE dtspsl_ci LIKE sCI  AND dtspsl_pin LIKE sPIN AND dtspsl_estado='A' and dtspsl_id = sidregistro) THEN 
    IF 'SI' = sestadoactivaciondigital THEN 
    SELECT COUNT(*) INTO cantidad
    FROM  _bp_datos_personales
    WHERE dtspsl_ci LIKE sCI   AND dtspsl_estado='A' AND dtspsl_id = sidregistro AND dtspsl_activaciond='SI' and dtspsl_pin=spin ;
     IF cantidad >=1 THEN
        SELECT dtspsl_fec_activaciond INTO FechActvacionDD  
        FROM  _bp_datos_personales
        WHERE dtspsl_ci LIKE sCI  AND dtspsl_pin LIKE sPIN and  dtspsl_activaciond LIKE 'SI' and dtspsl_estado='A' and dtspsl_pin=spin;
        confirmacio=('Activado digital en ==>' );     
        RETURN QUERY SELECT   ( confirmacio || FechActvacionDD);
      ELSE
         UPDATE _bp_datos_personales
         SET dtspsl_activaciond=sEstadoActivacionDigital,
         dtspsl_fec_activaciond=NOW(),
         dtspsl_modificado=NOW()         
         WHERE dtspsl_ci LIKE sCI  AND dtspsl_pin LIKE sPIN AND dtspsl_estado='A' AND dtspsl_activaciond LIKE 'NO' AND dtspsl_id = sidregistro; 
         confirmacio=('Activado digital hoy' );
         RETURN QUERY SELECT   confirmacio;
      END IF;
    ELSE
         SELECT COUNT(*) INTO cantidad
         FROM  _bp_datos_personales
         WHERE dtspsl_ci LIKE sCI   AND dtspsl_estado='A' AND dtspsl_id = sidregistro AND dtspsl_activaciond='NO' and dtspsl_pin=spin ;
         IF cantidad =0 THEN
            SELECT dtspsl_fec_activaciond INTO FechActvacionDD  
            FROM  _bp_datos_personales
            WHERE dtspsl_ci LIKE sCI  AND dtspsl_pin LIKE sPIN and  dtspsl_activaciond LIKE 'SI' and dtspsl_estado='A' and dtspsl_pin=spin;
            confirmacio=('Activado digital en ==>' );     
            RETURN QUERY SELECT   ( confirmacio || FechActvacionDD);
         ELSE
            UPDATE _bp_datos_personales
            SET dtspsl_activaciond='SI',
            dtspsl_fec_activaciond=NOW(),
            dtspsl_modificado=NOW()
            WHERE dtspsl_ci LIKE sCI  AND dtspsl_pin LIKE sPIN AND dtspsl_estado='A' AND dtspsl_activaciond LIKE 'NO' AND dtspsl_id = sidregistro; 
            confirmacio=('Activado digital hoy' );
            RETURN QUERY SELECT   confirmacio;
        END IF;   
     END IF; 
   ELSE
   confirmacio=('Ciudadano no registrado' );      
         RETURN QUERY SELECT   confirmacio;
   END IF;
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

--**********************ACTIVACION FISICA*************************
   
CREATE OR REPLACE FUNCTION spactivacionfisica(sci text, sestadoactivacionfisica character, sidregistro integer)
  RETURNS SETOF text AS
$BODY$
DECLARE
          cantidad integer;
          FechActvacionDD date;         
          confirmacio text;            
BEGIN 
   IF (  SELECT 1
    FROM  _bp_datos_personales
    WHERE dtspsl_ci LIKE sCI   AND dtspsl_estado='A' and dtspsl_id = sidregistro) THEN 
    IF 'SI' = sestadoactivacionfisica THEN 
    SELECT COUNT(*) INTO cantidad
    FROM  _bp_datos_personales
    WHERE dtspsl_ci LIKE sCI   AND dtspsl_estado='A' AND dtspsl_id = sidregistro AND dtspsl_activacionf='SI'  ;
     IF cantidad >=1 THEN
        SELECT dtspsl_fec_activacionf INTO FechActvacionDD  
        FROM  _bp_datos_personales
        WHERE dtspsl_ci LIKE sCI  AND   dtspsl_activacionf LIKE 'SI' and dtspsl_estado='A' ;
        confirmacio=('Activado fisica en ==>' );     
        RETURN QUERY SELECT   ( confirmacio || FechActvacionDD);
      ELSE
         UPDATE _bp_datos_personales
         SET dtspsl_activacionf=sestadoactivacionfisica,
         dtspsl_fec_activacionf=NOW(),
         dtspsl_modificado=NOW()         
         WHERE dtspsl_ci LIKE sCI  AND  dtspsl_estado='A' AND dtspsl_activacionf LIKE 'NO' AND dtspsl_id = sidregistro; 
         confirmacio=('Activado fisica hoy' );
         RETURN QUERY SELECT   confirmacio;
      END IF;
    ELSE
         SELECT COUNT(*) INTO cantidad
         FROM  _bp_datos_personales
         WHERE dtspsl_ci LIKE sCI   AND dtspsl_estado='A' AND dtspsl_id = sidregistro AND dtspsl_activacionf='NO'  ;
         IF cantidad =0 THEN
            SELECT dtspsl_fec_activacionf INTO FechActvacionDD  
            FROM  _bp_datos_personales
            WHERE dtspsl_ci LIKE sCI  AND  dtspsl_activacionf LIKE 'SI' and dtspsl_estado='A' ;
            confirmacio=('Activado fisica en ==>' );     
            RETURN QUERY SELECT   ( confirmacio || FechActvacionDD);
         ELSE
            UPDATE _bp_datos_personales
            SET dtspsl_activacionf='SI',
            dtspsl_fec_activacionf=NOW(),
            dtspsl_modificado=NOW()
            WHERE dtspsl_ci LIKE sCI  AND  dtspsl_estado='A' AND dtspsl_activacionf LIKE 'NO' AND dtspsl_id = sidregistro; 
            confirmacio=('Activado fisica hoy' );
            RETURN QUERY SELECT   confirmacio;
        END IF;   
     END IF; 
   ELSE
   confirmacio=('Ciudadano no registrado' );      
         RETURN QUERY SELECT   confirmacio;
   END IF;
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

--************************BLOQUEAR REGISTRO CIUDADANO**********************************	

CREATE OR REPLACE FUNCTION spbloquearregistrociudadano(
    IN dtspsidregistrosl_id integer,
    IN sobservacion text,
    IN sidusuario integer)
  RETURNS SETOF text AS
$BODY$
DECLARE
        
          count integer;                   
          ciudadano TEXT;          
          confirmacio text;
         
          
BEGIN    

    SELECT COUNT(*) INTO count
    FROM  _bp_datos_personales
    WHERE dtspsl_id =dtspsIdRegistrosl_id AND (dtspsl_estado_activacion LIKE 'N' OR dtspsl_estado_activacion LIKE ' '  );

    IF count >=1 THEN 
      confirmacio=('BLOQUEADO HOY' );
	UPDATE _bp_datos_personales
        SET dtspsl_estado_activacion='B',        
         dtspsl_modificado=NOW(),
         dtspsl_usr_id=sIdusuario,
         dtspsl_observacion_activacion=sObservacion
        WHERE dtspsl_id =dtspsIdRegistrosl_id AND dtspsl_estado='A';      
      RETURN QUERY SELECT   confirmacio;
    ELSE 
     SELECT (dtspsl_nombres ||' ' || dtspsl_paterno ||' ' || dtspsl_materno) INTO ciudadano  
      FROM  _bp_datos_personales
      WHERE dtspsl_id =dtspsIdRegistrosl_id and  dtspsl_estado_activacion LIKE 'B' and dtspsl_estado='A';
       confirmacio=('BLOQUEADO CIUDADANO ==> ' );      
      RETURN QUERY SELECT   ( confirmacio || ciudadano);      
     END IF;  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--SELECT *  FROM spBloquearRegistroCiudadano(1,'PREUBA',1);
--***********************************DESBLOQUEAR REGISTRO CIUDADANO*******************

CREATE OR REPLACE FUNCTION spbloquearregistrociudadano(dtspsidregistrosl_id integer, sobservacion text, sidusuario integer)
  RETURNS SETOF text AS
$BODY$
DECLARE
        
          count integer;                   
          ciudadano TEXT;          
          confirmacio text;
         
          
BEGIN    

    SELECT COUNT(*) INTO count
    FROM  _bp_datos_personales
    WHERE dtspsl_id =dtspsIdRegistrosl_id AND (dtspsl_estado_activacion LIKE 'N' OR dtspsl_estado_activacion LIKE ' '  );

    IF count >=1 THEN 
      confirmacio=('Bloqueado hoy' );
  UPDATE _bp_datos_personales
        SET dtspsl_estado_activacion='B',        
         dtspsl_modificado=NOW(),
         dtspsl_usr_id=sIdusuario,
         dtspsl_observacion_activacion=sObservacion
        WHERE dtspsl_id =dtspsIdRegistrosl_id AND dtspsl_estado='A';      
      RETURN QUERY SELECT   confirmacio;
    ELSE 
     SELECT (dtspsl_nombres ||' ' || dtspsl_paterno ||' ' || dtspsl_materno) INTO ciudadano  
      FROM  _bp_datos_personales
      WHERE dtspsl_id =dtspsIdRegistrosl_id and  dtspsl_estado_activacion LIKE 'B' and dtspsl_estado='A';
       confirmacio=('Bloqueado ciudadano ==> ' );      
      RETURN QUERY SELECT   ( confirmacio || ciudadano);      
     END IF;  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


  --  FUNCTION spbuscarci(text);

CREATE OR REPLACE FUNCTION spbuscarci(IN sci text)
  RETURNS SETOF text AS
$BODY$
BEGIN    
    RETURN QUERY SELECT    dtspsl_ci
     FROM _bp_datos_personales
                 WHERE   dtspsl_ci LIKE sCi and dtspsl_estado='A'  ;       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  --************************BUSCAR COOREO
CREATE OR REPLACE FUNCTION spbuscarcorreo(IN scorreo text)
  RETURNS TABLE(vnombre text, vci text) AS
$BODY$
BEGIN 
     
      RETURN QUERY select (dtspsl_nombres|| ' '||dtspsl_paterno || ' '||dtspsl_materno ) , dtspsl_ci
                   from _bp_datos_comunicacion
                   INNER JOIN _bp_datos_personales  ON dtscmc_id_dtspersonal=dtspsl_id and dtspsl_estado='A' 
                   where 
                   ((scorreo IS NULL)OR(dtscmc_referencia LIKE '%'||scorreo||'%'))

                   AND dtscmc_estado='A';  
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--*********************************BUSCAR REGISTRO CIUDADANO*********************  
CREATE OR REPLACE FUNCTION spbuscarregistromedico(IN prsid integer)
  RETURNS TABLE(dtsmdcid integer, dtsmdcdtspslid integer, dtsmdcgrpsanguineo text, dtsmdcsbtmedica text, dtsmdcfechaalta date, dtsmdcantpersonal_1 text, dtsmdcantpersonal_2 text, dtsmdcantpersonal_3 text, dtsmdcantfamiliar_1 text, dtsmdcantfamiliar_2 text, dtsmdcantfamiliar_3 text, dtsmdcobservaciones text) AS
$BODY$
BEGIN    
    RETURN QUERY select dtsmdc_id, dtsmdc_dtspsl_id, dtsmdc_grp_sanguineo, dtsmdc_cbt_medica, dtsmdc_fecha_alta, dtsmdc_ant_personal_1, 
    dtsmdc_ant_personal_2, dtsmdc_ant_personal_3, dtsmdc_ant_familiar_1, dtsmdc_ant_familiar_2, dtsmdc_ant_familiar_3, dtsmdc_observaciones
    from _bp_datos_medicos 
    where dtsmdc_dtspsl_id=prsId and dtsmdc_estado<>'B';      
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

--  FUNCTION spbusquedaformulario(integer);

CREATE OR REPLACE FUNCTION spbusquedaformulario(IN sidciudadano integer)
  RETURNS TABLE(vtra_id integer, vdvser_id integer, vid_ciudadano integer, vfecha text, venviado text, vid_usuario integer, vregistrado timestamp without time zone, vmodificado timestamp without time zone, vestado character, vservicio text) AS
$BODY$
BEGIN    
    RETURN QUERY  select frm_tra_id,frm_tra_dvser_id,frm_tra_id_ciudadano,frm_tra_fecha,frm_tra_enviado,frm_tra_id_usuario,frm_tra_registrado,frm_tra_modificado,frm_tra_estado, serdv_servicio 
  from _formulario_tramites
  inner join _servicio_div ON serdv_id = frm_tra_dvser_id
  where frm_tra_estado='A' AND frm_tra_id_ciudadano=sidciudadano
  order by frm_tra_id desc;       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  -- DROP FUNCTION spbusquedaformulario(integer, integer);

CREATE OR REPLACE FUNCTION spbusquedaformulario(IN sserid integer, IN sidciudadano integer)
  RETURNS TABLE(vtra_id integer, vdvser_id integer, vid_ciudadano integer, vfecha text, venviado text, vid_usuario integer, vregistrado timestamp without time zone, vmodificado timestamp without time zone, vestado character, vservicio text) AS
$BODY$
BEGIN    
    RETURN QUERY  select frm_tra_id,frm_tra_dvser_id,frm_tra_id_ciudadano,frm_tra_fecha,frm_tra_enviado,frm_tra_id_usuario,frm_tra_registrado,frm_tra_modificado,frm_tra_estado, serdv_servicio  
  from _formulario_tramites
  inner join _servicio_div ON serdv_id = frm_tra_dvser_id
  where frm_tra_estado='A' AND frm_tra_dvser_id=sserid AND frm_tra_id_ciudadano=sidciudadano
  order by frm_tra_id desc ;       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--*********************************CAMBIO DE ESTADO REGISTRO CIUDADANO********************   
  
CREATE OR REPLACE FUNCTION spcambioestado(id integer, estado_activacion text)
  RETURNS SETOF text AS
$BODY$
DECLARE            
          confirmacio text;       
          
BEGIN    
    --confirmacio=('DESBLOQUEADO' );
   IF estado_activacion = 'BLOQUEADO' 
     THEN
        confirmacio=('Desbloqueado' );
  UPDATE _bp_datos_personales 
        SET dtspsl_estado_activacion = 'DESBLOQUEADO'  
        WHERE dtspsl_id = id AND dtspsl_estado_activacion='BLOQUEADO' AND dtspsl_estado='A';     
   END IF; 
  IF estado_activacion = 'DESBLOQUEADO' 
  THEN
      UPDATE _bp_datos_personales 
      SET dtspsl_estado_activacion = 'BLOQUEADO'  
      WHERE dtspsl_id = id AND dtspsl_estado_activacion='DESBLOQUEADO' AND dtspsl_estado='A';
      confirmacio=('Bloqueado');
  END IF;
   RETURN QUERY SELECT   confirmacio;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
--***********************AUTENTICACION REGISTRO CIDADANO***************************************

CREATE OR REPLACE FUNCTION spautenticacionciudadano(IN sci text, IN spin text)
  RETURNS TABLE(vid integer, vnombres text, vpaterno text, vmaterno text, vactivacionf character, vactivaciond character, vci text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT dtspsl_id,dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_activacionf,dtspsl_activaciond,dtspsl_ci
     FROM _bp_datos_personales
     where dtspsl_ci=sci  AND dtspsl_pin=spin AND dtspsl_estado='A';       
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


--*****************************buscar ciudadano

CREATE OR REPLACE FUNCTION spmostrardatosperosnales(IN idprs integer)
  RETURNS TABLE(vid integer, vid_civil integer, vid_tipo_registro integer, vci text, vpin text, vnombres text, paterno text, vmaterno text, vsexo character, vocupacion text, vfecha_na date, vid_tipopersona text, vid_poderrep text, vid_nrodocumento text, vid_nronotaria text, vid_razonsocial text, vid_nit text, vdireccion text, vidcorreo integer, vcorreo text, vidtelefono integer, vtelefono text, vidmovil integer, vmovil text) AS
$BODY$
BEGIN    
    RETURN  QUERY 
    select dtspsl_id,dtspsl_id_estado_civil,dtspsl_id_tp_registro,dtspsl_ci,dtspsl_pin,dtspsl_nombres,dtspsl_paterno, dtspsl_materno,dtspsl_sexo,dtspsl_ocupacion,dtspsl_fec_nacimiento,
    dtspsl_tipo_persona,dtspsl_poder_replegal,dtspsl_nro_documento,dtspsl_nro_notaria,dtspsl_razon_social,dtspsl_nit,
   (select dtsdrc_direccion  from _bp_datos_direccion  where dtsdrc_id_dtspesonales=idprs  and dtsdrc_estado='A'  ORDER BY dtsdrc_id DESC limit 1   ) direccion,
    (select dtscmc_id_tpcomunicacion  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 1 ORDER BY dtscmc_id asc limit 1   ) idcorreo ,
   (select dtscmc_referencia  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs  AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 1 ORDER BY dtscmc_id DESC limit 1   ) correo ,
   (select dtscmc_id_tpcomunicacion  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs  AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 2 ORDER BY dtscmc_id DESC limit 1   ) idtelefono ,
   (select dtscmc_referencia  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs  AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 2 ORDER BY dtscmc_id DESC limit 1   ) telefono ,
   (select dtscmc_id_tpcomunicacion  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs  AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 3 ORDER BY dtscmc_id DESC limit 1   ) idmovil ,
   (select dtscmc_referencia  from _bp_datos_comunicacion  inner join _bp_tipo_comunicacion on tpcmcc_id =dtscmc_id_tpcomunicacion AND tpcmcc_estado='A'  where dtscmc_id_dtspersonal=idprs  AND dtscmc_estado='A' and dtscmc_id_tpcomunicacion = 3 ORDER BY dtscmc_id DESC limit 1   ) movil 
   from _bp_datos_personales
   where dtspsl_id=idprs  and dtspsl_estado='A';
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  
-------*************************   -----------  
CREATE OR REPLACE FUNCTION splistadocrecibidos(IN sidper integer)
  RETURNS TABLE(vid_doc integer, vdescripcion_doc text, vresumen_doc text, vurl_doc text, vdescripcion_ctg text) AS
$BODY$
BEGIN 
     
      RETURN QUERY SELECT docrbds_id,docrbds_descripcion,docrbds_resumen,docrbds_url, ctgr_descripcion FROM _bp_doc_recibidos
             INNER JOIN _bp_categorias ON docrbds_id_categoria=ctgr_id 
                   WHERE docrbds_id_dtspesonales=sidper;   
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  
  --  FUNCTION splistafrmdatos(integer, integer, integer);

CREATE OR REPLACE FUNCTION splistafrmdatos(IN sidciudadano integer, IN sidservicio integer, IN sidtramite integer)
  RETURNS TABLE(form_id integer, form_ser_id integer, form_contenido text) AS
$BODY$
BEGIN 
     
      RETURN QUERY SELECT frm_id, frm_dvser_id, frm_dato FROM _formulario_datos
          WHERE frm_id_ciudadano = sidciudadano AND frm_estado = 'A' AND frm_dvser_id = sidservicio AND frm_id_tramite = sidtramite
          ORDER By frm_id desc
          LIMIT 1;   
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
  -- DROP FUNCTION splistafrmdatos(integer);

CREATE OR REPLACE FUNCTION splistafrmdatos(IN sidciudadano integer)
  RETURNS TABLE(form_id integer, form_ser_id integer, form_contenido text) AS
$BODY$
BEGIN 
     
      RETURN QUERY SELECT frm_id, frm_dvser_id, frm_dato FROM _formulario_datos
          WHERE frm_id_ciudadano = sidciudadano AND frm_estado = 'A'
          ORDER By frm_id desc
          LIMIT 1;   
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- DROP FUNCTION splistafrmdatos(integer, integer);

CREATE OR REPLACE FUNCTION splistafrmdatos(IN sidciudadano integer, IN sidservicio integer)
  RETURNS TABLE(form_id integer, form_ser_id integer, form_contenido text) AS
$BODY$
BEGIN 
     
      RETURN QUERY SELECT frm_id, frm_dvser_id, frm_dato FROM _formulario_datos
          WHERE frm_id_ciudadano = sidciudadano AND frm_estado = 'A' AND frm_dvser_id = sidservicio 
          ORDER By frm_id desc
          LIMIT 1;   
         
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- funcon splistaposicion -----
CREATE OR REPLACE FUNCTION splistaposicion(IN spblpsc_posicion integer, IN spblcal_id_ciudadano integer)
  RETURNS TABLE(vpblpsc_id integer, vpblpsc_pblcat_id integer, vpblpsc_posicion text, vpblpsc_alto double precision, vpblpsc_ancho double precision, vpblpsc_registrado timestamp without time zone, vpblpsc_modificado timestamp without time zone, vpblpsc_usr_id integer, vpblcal_id integer, vpblcal_id_usuario integer, vpblcal_pblpsc_id integer, vpblcal_tipo text, vpblcal_fech_inicio date, vpblcal_fech_fin date, vpblcal_hora_inicio time without time zone, vpblcal_hora_fin time without time zone, vpblcal_registrado timestamp without time zone, vpblcal_modificado timestamp without time zone, vpblcal_estado character, vpblcal_id_ciudadano integer, vpblcat_descripcion text) AS
$BODY$
BEGIN 
  IF spblpsc_posicion <> 0 THEN 
      IF spblcal_id_ciudadano <> 0 THEN     
        RETURN QUERY select   pblpsc_id,
          pblpsc_pblcat_id ,
          pblpsc_posicion ,
          pblpsc_alto,
          pblpsc_ancho ,
          pblpsc_registrado ,
          pblpsc_modificado,
          pblpsc_usr_id,  
          pblcal_id serial ,
          pblcal_id_usuario ,
          pblcal_pblpsc_id,
          pblcal_tipo ,
          pblcal_fech_inicio ,
          pblcal_fech_fin,
          pblcal_hora_inicio ,
          pblcal_hora_fin,
          pblcal_registrado,
          pblcal_modificado,
          pblcal_estado,
          pblcal_id_ciudadano,
          pblcat_descripcion
          from _publicidad_calendario 
        inner join _publicidad_posicion on pblcal_pblpsc_id = pblpsc_id and pblcal_estado = 'A'
        inner join _publicidad_categoria on pblcat_id = pblpsc_pblcat_id and pblpsc_estado = 'A'
        where pblpsc_estado='A' AND pblpsc_id=spblpsc_posicion AND pblcal_id_ciudadano = spblcal_id_ciudadano;
      ELSE
        RETURN QUERY select   pblpsc_id,
          pblpsc_pblcat_id ,
          pblpsc_posicion ,
          pblpsc_alto,
          pblpsc_ancho ,
          pblpsc_registrado ,
          pblpsc_modificado,
          pblpsc_usr_id,  
          pblcal_id serial ,
          pblcal_id_usuario ,
          pblcal_pblpsc_id,
          pblcal_tipo ,
          pblcal_fech_inicio ,
          pblcal_fech_fin,
          pblcal_hora_inicio ,
          pblcal_hora_fin,
          pblcal_registrado,
          pblcal_modificado,
          pblcal_estado,
          pblcal_id_ciudadano,
          pblcat_descripcion
          from _publicidad_calendario 
        inner join _publicidad_posicion on pblcal_pblpsc_id = pblpsc_id and pblcal_estado = 'A'
        inner join _publicidad_categoria on pblcat_id = pblpsc_pblcat_id and pblpsc_estado = 'A'
        where pblpsc_estado='A' AND pblpsc_id=spblpsc_posicion;
      END IF;
  ELSE
    RETURN QUERY select   pblpsc_id,
      pblpsc_pblcat_id ,
      pblpsc_posicion ,
      pblpsc_alto,
      pblpsc_ancho ,
      pblpsc_registrado ,
      pblpsc_modificado,
      pblpsc_usr_id,  
      pblcal_id serial ,
      pblcal_id_usuario ,
      pblcal_pblpsc_id,
      pblcal_tipo ,
      pblcal_fech_inicio ,
      pblcal_fech_fin,
      pblcal_hora_inicio ,
      pblcal_hora_fin,
      pblcal_registrado,
      pblcal_modificado,
      pblcal_estado,
      pblcal_id_ciudadano,
      pblcat_descripcion
      from _publicidad_calendario 
    inner join _publicidad_posicion on pblcal_pblpsc_id = pblpsc_id and pblcal_estado = 'A'
    inner join _publicidad_categoria on pblcat_id = pblpsc_pblcat_id and pblpsc_estado = 'A'
    where pblpsc_estado='A' AND pblcal_id_ciudadano = spblcal_id_ciudadano;   
  END IF;   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
-- DROP FUNCTION splogciudadano(integer);

CREATE OR REPLACE FUNCTION splogciudadano(IN susuarioid integer)
  RETURNS TABLE(vnombres text, vpaterno text, vmaterno text, vtipo_persona text, vrazon_social text, vevento text, vusr_id integer, vlgsusr_id integer, vdtspsl_id integer, vlgsregistrado timestamp without time zone, vlgsmodificado timestamp without time zone) AS
$BODY$
DECLARE
        
          i integer;
          j integer;
          con integer;
BEGIN    
     i := 1;
    CREATE LOCAL TEMPORARY TABLE "logciudadano" (
     id serial PRIMARY KEY, snombres text,spaterno text,smaterno text,stipo_persona text,srazon_social text,sevento text ,susr_id integer,slgsusr_id integer ,sdtspsl_id integer,slgs_registrado timestamp,slgs_modificado timestamp  ) WITH OIDS;
    INSERT INTO logciudadano
    (snombres ,spaterno ,smaterno ,stipo_persona ,srazon_social ,sevento  ,susr_id ,slgsusr_id  ,sdtspsl_id,slgs_registrado,slgs_modificado  )  
    (select dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_tipo_persona,dtspsl_razon_social,lgs_evento,lgs_id_log,lgs_usr_id,lgs_dtspsl_id,lgs_registrado,lgs_modificado from _bp_logs_eventos
     inner join _bp_datos_personales  on lgs_dtspsl_id=dtspsl_id 
     where lgs_usr_id=susuarioid and lgs_estado='A' AND dtspsl_estado='A');
      
     CREATE LOCAL TEMPORARY TABLE "logeventos" (
     id serial PRIMARY KEY,
     sevento text ,susr_id integer,slgsusr_id integer,sdtspsl_id integer,slgs_registrado timestamp,slgs_modificado timestamp   ) WITH OIDS;
     INSERT INTO logeventos
    (sevento  ,susr_id ,slgsusr_id ,sdtspsl_id,slgs_registrado ,slgs_modificado )  
    (select lgs_evento,lgs_id_log,lgs_usr_id,lgs_dtspsl_id,lgs_registrado,lgs_modificado from _bp_logs_eventos     
     where lgs_usr_id=susuarioid AND lgs_dtspsl_id=0 and lgs_estado='A');
     
     WHILE (i<=(SELECT COUNT(*)FROM logeventos)) 
     LOOP 
     INSERT INTO logciudadano
     (sevento,susr_id,slgsusr_id, sdtspsl_id,slgs_registrado,slgs_modificado)   
    (SELECT  g.sevento,g.susr_id,g.slgsusr_id,g.sdtspsl_id,g.slgs_registrado,g.slgs_modificado  FROM logeventos as g  WHERE g.id=i) ;     
     i = i+1;
     END LOOP;     
    RETURN QUERY SELECT snombres ,spaterno ,smaterno ,stipo_persona ,srazon_social ,sevento  ,susr_id ,slgsusr_id  ,sdtspsl_id ,slgs_registrado ,slgs_modificado  
    FROM "logciudadano"  order by slgs_registrado DESC; 
    drop table logciudadano;
     drop table logeventos;
    
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- DROP FUNCTION splogciudadano_j_n(integer, integer);

CREATE OR REPLACE FUNCTION splogciudadano_j_n(IN susuarioid integer, IN susciudadanoid integer)
  RETURNS TABLE(vnombres text, vpaterno text, vmaterno text, vtipo_persona text, vrazon_social text, vevento text, vusr_id integer, vlgsusr_id integer, vdtspsl_id integer, vlgsregistrado timestamp without time zone, vlgsmodificado timestamp without time zone) AS
$BODY$
DECLARE
        
          i integer;
          j integer;
          con integer;
BEGIN    
     i := 1;
    CREATE LOCAL TEMPORARY TABLE "logciudadano" (
     id serial PRIMARY KEY, snombres text,spaterno text,smaterno text,stipo_persona text,srazon_social text,sevento text ,susr_id integer,slgsusr_id integer ,sdtspsl_id integer,slgs_registrado timestamp,slgs_modificado timestamp  ) WITH OIDS;
    INSERT INTO logciudadano
    (snombres ,spaterno ,smaterno ,stipo_persona ,srazon_social ,sevento  ,susr_id ,slgsusr_id  ,sdtspsl_id,slgs_registrado,slgs_modificado  )  
    (select dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_tipo_persona,dtspsl_razon_social,lgs_evento,lgs_id_log,lgs_usr_id,lgs_dtspsl_id,lgs_registrado,lgs_modificado from _bp_logs_eventos
     inner join _bp_datos_personales  on lgs_dtspsl_id=dtspsl_id 
     where lgs_usr_id=susuarioid and lgs_dtspsl_id =susciudadanoid and lgs_estado='A' AND dtspsl_estado='A');
      
     CREATE LOCAL TEMPORARY TABLE "logeventos" (
     id serial PRIMARY KEY,
     sevento text ,susr_id integer,slgsusr_id integer,sdtspsl_id integer,slgs_registrado timestamp,slgs_modificado timestamp   ) WITH OIDS;
     INSERT INTO logeventos
    (sevento  ,susr_id ,slgsusr_id ,sdtspsl_id,slgs_registrado ,slgs_modificado )  
    (select lgs_evento,lgs_id_log,lgs_usr_id,lgs_dtspsl_id,lgs_registrado,lgs_modificado from _bp_logs_eventos     
     where lgs_usr_id=susuarioid and lgs_dtspsl_id =susciudadanoid AND lgs_dtspsl_id=0 and lgs_estado='A');
     
     WHILE (i<=(SELECT COUNT(*)FROM logeventos)) 
     LOOP 
     INSERT INTO logciudadano
     (sevento,susr_id,slgsusr_id, sdtspsl_id,slgs_registrado,slgs_modificado)   
    (SELECT  g.sevento,g.susr_id,g.slgsusr_id,g.sdtspsl_id,g.slgs_registrado,g.slgs_modificado  FROM logeventos as g  WHERE g.id=i) ;     
     i = i+1;
     END LOOP;     
    RETURN QUERY SELECT snombres ,spaterno ,smaterno ,stipo_persona ,srazon_social ,sevento  ,susr_id ,slgsusr_id  ,sdtspsl_id ,slgs_registrado ,slgs_modificado  
    FROM "logciudadano"  order by slgs_registrado DESC; 
    drop table logciudadano;
     drop table logeventos;
    
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;


 --*********************       ********************

CREATE OR REPLACE FUNCTION spbuscarregistroempresa(IN snit text, IN snombreempresa text, IN scirepresentante text, IN snombrerepresentante text, IN smaternorepresentante text, IN spaternorepresentante text)
  RETURNS TABLE(vid integer, vci text, vnombre text, vappaterno text, vapmaterno text, vfechanacimiento date, vactivacionf character, vactivaciond character, vfec_activacionf date, vfec_activaciond date, vobservacion_activacion text, vestado_activacion text, vnit text, vrazonsocial text, vtipo_persona text) AS
$BODY$
BEGIN    
    RETURN QUERY SELECT   dtspsl_id, dtspsl_ci,dtspsl_nombres,dtspsl_paterno,dtspsl_materno,dtspsl_fec_nacimiento,dtspsl_activacionf,dtspsl_activaciond,dtspsl_fec_activacionf,dtspsl_fec_activaciond,dtspsl_observacion_activacion,dtspsl_estado_activacion, dtspsl_nit, dtspsl_razon_social,dtspsl_tipo_persona
     FROM _bp_datos_personales
                 WHERE  dtspsl_estado='A' 
                 AND ((snit IS NULL)OR(dtspsl_nit LIKE '%'||snit||'%'))
                 AND ((snombreempresa IS NULL)OR(dtspsl_razon_social LIKE '%'||snombreempresa||'%'))
                 AND ((scirepresentante IS NULL)OR(dtspsl_ci LIKE '%'||scirepresentante||'%'))  
                 AND ((snombrerepresentante IS NULL)OR(dtspsl_nombres LIKE '%'||snombrerepresentante||'%')) 
                 AND ((smaternorepresentante IS NULL)OR(dtspsl_materno LIKE '%'||smaternorepresentante||'%')) 
                 AND ((spaternorepresentante IS NULL)OR(dtspsl_paterno LIKE '%'||spaternorepresentante||'%')) 
                 AND dtspsl_tipo_persona = 'J';
                     
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

--  FUNCTION spregistradocumentosciu(integer, text, text, text, text, integer, integer);

CREATE OR REPLACE FUNCTION spregistradocumentosciu(sid_dtspesonales integer, sci text, sdescripcion text, sresumen text, surl text, sid_categoria integer, susr_id integer)
  RETURNS SETOF text AS
$BODY$
DECLARE            
          confirmacio text;       
          
BEGIN    
   INSERT INTO _bp_doc_recibidos (docrbds_id_dtspesonales,docrbds_ci, docrbds_descripcion,docrbds_resumen,docrbds_url,docrbds_id_categoria, docrbds_usr_id) VALUES (sid_dtspesonales,sci,sdescripcion, sresumen, surl,sid_categoria,susr_id);
   confirmacio='Documentos registrados';
   RETURN QUERY SELECT   confirmacio;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
-- noticiaslistar --

CREATE OR REPLACE FUNCTION noticiaslst()
  RETURNS TABLE(cntid integer, cnttitulo text, cntresumen text, cnturl text, cntimagen text, cntidusuario integer, cntregistrado timestamp without time zone, cntmodificado timestamp without time zone, cntestado character) AS
$BODY$
BEGIN    
    RETURN QUERY select cnt_id as  "cnt_id",cnt_titulo as  "cnt_titulo",cnt_resumen  as  "cnt_resumen",cnt_url  as  "cnt_url" ,cnt_imagen  as  "cnt_imagen",
      cnt_id_usuario  as  "cnt_id_usuario",cnt_registrado as "cnt_registrado", cnt_modificado  as  "cnt_modificado",cnt_estado  as  "cnt_estado"
      from _ciudadano_noticias
      where cnt_estado <> 'B' and cnt_visible='SI' 
      order by cnt_id desc
;     
   
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
 --  archivos  listar
CREATE OR REPLACE FUNCTION archciudadanolst(IN idciudadano integer, IN proceso text)
  RETURNS TABLE(crchidarchivo integer, crchciudadano_id integer, crchproceso text, crchactividad text, crchnombre text, crchtamano text, crchdireccion text) AS
$BODY$
BEGIN    
    RETURN QUERY   select crch_id_archivo as  "crch_id_archivo",crch_ciudadano_id as "crch_ciudadano_id",crch_proceso as "crch_proceso",crch_actividad as "crch_actividad",
	 crch_nombre as "crch_nombre",crch_tamano as "crch_tamano", crch_direccion as "crch_direccion"
  from _ciudadano_archivos
  where crch_ciudadano_id = idCiudadano and crch_proceso = proceso and crch_estado <> 'B'
  order by crch_id_archivo asc
;      
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;



