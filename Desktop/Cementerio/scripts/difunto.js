app.controller('difuntoController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
    };
    //listar difunto
    $scope.getDifunto = function(){
        $.blockUI();
        var resDifunto = {
            "procedure_name":"fn_lstdifuntos"
        };
        //servicio listar difuntos
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resDifunto);
        obj.success(function (response) {
            $scope.obtDifunto = response;
            var data = response;   //grabamos la respuesta para el paginado
            console.log("el data es:" +  data);
            $scope.tablaDifunto = new ngTableParams({
                page: 1,          
                count: 5,
                filter: {},
                sorting: {}      
            }, {
                total: $scope.obtDifunto.length,
                getData: function($defer, params) {
                    var filteredData = params.filter() ?
                    $filter('filter')($scope.obtDifunto, params.filter()) :
                    $scope.obtDifunto;             
                    var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    $scope.$scope.obtDifunto;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                 
                }
            });
            $.unblockUI();            
        })
        obj.error(function(error) {
            $scope.errors["error_difunto"] = error;  
            $.unblockUI();            
        });        
    };
    //estados nacionalidad
   /*  $scope.getEstado_nacionalidad = function(){
        var resEstadoNacionalidad = {
            "procedure_name":"fn_lst_nacionalidad"
        };        
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoNacionalidad)
        .success(function (response) {
            $scope.estados_nacionalidad = response;  
            console.log("esta es la respuesta:" + response);         
        }).error(function(error) {
            $scope.errors["error_reg_nacionalidad"] = error;            
        });
    }
    //fin nacionalidad
    //obteniendo localidad
    $scope.getEstado_localidad = function(){
        var resEstadoLocalidad={
            "procedure_name":"fn_lst_localidad"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoLocalidad)
        .success(function (response){
            $.scope.estados_localidad = response;
        });
        .error(function(error){
            $scope.errors["error_reg_localidad"]=error;
        });
    }
    //fin
    //obteniendo provincia
    $scope.getEstado_provincia = function(){
        var resEstadoProvincia={
            "procedure_name":"fn_lst_provincia"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoProvincia)
        .success(function (response){
            $.scope.estados_provincia = response;
        });
        .error(function(error){
            $scope.errors["error_reg_provincia"]=error;
        });
    }
    //fin
    //obteniendo profesion
    $scope.getEstado_profesion = function(){
        var resEstadoProfesion={
            "procedure_name":"fn_lst_profesion"
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resEstadoProfesion)
        .success(function (response){
            $.scope.estados_profesion = response;
        });
        .error(function(error){
            $scope.errors["error_reg_profesion"]=error;
        });
    }
    //fin
    $scope.adicionarDifunto = function(datosDifunto){
        $.blockUI();
        var difunto={};
        var fechaNacimiento=datosDifunto.dif_fechanac.getFullYear() + "-" + datosDifunto.dif_fechanac.getMonth() + "-" + datosDifunto.dif_fechanac.getDate() + " " + datosDifunto.dif_fechanac.getHours() + ":" + datosDifunto.dif_fechanac.getMinutes() + ":" + datosDifunto.dif_fechanac.getSeconds();
        difunto['dif_nombre'] = datosDifunto.difnom;
        difunto['dif_ap_pat'] = datosDifunto.difpat;
        difunto['dif_ap_mat'] = datosDifunto.difmat;
        difunto['dif_otro_ap'] = datosDifunto.difotroap;
        difunto['dif_ci'] = datosDifunto.difcarnetidentidad,
        difunto['nac_id'] = datosDifunto.nacionalidad_id;
        difunto['loc_cod'] = datosDifunto.localidad_id;
        difunto['prov_id'] = datosDifunto.provincia_id;
        difunto['dif_oriciu'] = datosDifunto.diforigen_cuidad;
        difunto['dif_fechanac'] = datosDifunto.dif_fechaNacimiento;
        difunto['dif_sexo'] = datosDifunto.difsexo;
        difunto['dif_est_civil']= datosDifunto.difEstadoCivil;
        difunto['prof_id'] = datosDifunto.profesion_id;
        difunto['dif_observacion'] = datosDifunto.datosDifunto.difObservacion;
        difunto['dif_gestion'] = datosDifunto.difGestion;
        difunto['dif_edad'] = datosDifunto.difEdad;
        difunto['dif_fcharegistrado'] = fechactual;
        difunto['dif_modificado'] = fechactual;
        difunto['dif_estado'] = 'ACTIVO';

        var resDifunto={
            table_name:"sat_difunto",
            body:difunto
        };
        //servicio insertar difunto
          var obj=DreamFactory.api[CONFIG.SERVICE].createRecords(resPersona);
        obj.success(function(data){
            //$scope.getEstados();
            
            $.unblockUI(); 
            sweet.show('', 'Registro insertado', 'success');
            $route.reload();
            $scope.getDifunto();
        })
        .error(function(data){
            sweet.show('', 'Registro no insertado', 'error');
        })
    };
    $scope.eliminarPersonaCargar = function(persona){
      /*  $scope.desabilitado=true;
        $scope.datosPersona = persona;
        $scope.boton="del";
        $scope.titulo="Eliminar Personas";
    };*/





     $scope.modificarPersonaCargar = function(persona){
       /*$scope.desabilitado=false;
        $scope.datosDifuntos = persona;
        $scope.boton="upd";
        $scope.titulo="Modificar Personas";*/
    };





      $scope.limpiar = function(){
      /*  $scope.datosDifunto='';
        $scope.desabilitado=false;
        $scope.boton="new";
        $scope.titulo="Registro de difuntos";
        alert("esta llamando a la funcion limpiar");
    };*/
    };
    //iniciamos controlador 

     $scope.$on('api:ready',function(){
        $scope.getDifunto();
      /* $scope.getEstado_nacionalidad();
       $scope.getEstado_localidad();
       $scope.getEstado_provincia();
        $scope.getEstado_profesion();*/
    });

     $scope.inicioDifunto = function () {
        if(DreamFactory.isReady()){
            $scope.getDifunto();
            /*$scope.getEstado_nacionalidad();
            $scope.getEstado_localidad();
            $scope.getEstado_provincia();
            $scope.getEstado_profesion();*/
        }
    };  
};  